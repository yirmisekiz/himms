function sweetConfirm(title, text) {
    event.preventDefault();
    var target = $(event.target);
    var href = null;
    var form = null;

    if (target.is("a")) href = target.attr("href");
    else if (target.is("button:submit")) form = target.closest("form");
    else if (target.is("button")) href = target.attr("href") || target.attr("value");

    swal({
        title: title,
        text: text,
        type: "warning",
        allowOutsideClick: true,
        showCancelButton: true,
        confirmButtonText: "Evet",
        cancelButtonText: "Vazgeç",
    }, function () {
        if (href) window.location.href = href;
        else if (form) form.submit();
    });
}

$(document).ready(function () {

    var main = {
        dropzone: function () {
            Dropzone.prototype.defaultOptions.dictRemoveFile = "Fotoğrafı Sil";
        },
        sweetalert: function () {
            if ($('#js-swal-msg').val()) {
                swal({
                    title: $('#js-swal-msg').data('title'),
                    text: $('#js-swal-msg').val(),
                    type: $('#js-swal-msg').data('type'),
                    confirmButtonText: 'Tamam'
                })
            }
        },
        datepickers: function () {
            $('#news-datepicker .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: "dd/mm/yyyy"
            });
        },
        selectBoxes: function () {
            $(".js-select-collection").select2({
                placeholder: "Seçiniz",
            });

            $(".js-select-age").select2({
                placeholder: "Seçiniz",
            });

            if ($('#js-color-options').length > 0) {
                var colorOptions = [];
                $.each(jQuery.parseJSON($('#js-color-options').val()), function ($i, $e) {
                    var html = '<div style="display:block"><div style="height: 25px;width: 25px;border: 1px solid ' + $e.hex_code + ';border-radius: 50%;background: ' + $e.hex_code + ';display: inline-block;"></div><span style="background-color:' + $e.hex_code + '"></span><div>' + $e.name + '</div></div>';
                    colorOptions.push({html: html, text: $e.name, id: $e.id});
                });

                function template(colorOptions) {
                    return colorOptions.html;
                }

                function formatColor(color) {
                    var rgb = $($(color.html).find('div')[0]).css('background');
                    return '<span style="color:' + rgb + '">' + color.text + '</span>';
                }

                $(".js-select-colors").select2({
                    data: colorOptions,
                    templateResult: template,
                    templateSelection: formatColor,
                    escapeMarkup: function (m) {
                        return m;
                    }
                });
            }

        }
    };

    main.dropzone();
    main.sweetalert();
    main.datepickers();
    main.selectBoxes();

});