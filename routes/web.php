<?php

Auth::routes();

Route::prefix('{lang?}')->middleware('locale')->group(function () {

    Route::get('/', function () {

        return view('public.mainpage');
    });

    Route::post('/addSubscription', 'PagesController@addSubscription');
    Route::post('/sendEmail', 'PagesController@sendEmail');
    Route::get('/collections/{collectionSlug}/{collectionId}', 'PagesController@showCollection');
    Route::get('/collections/{collectionSlug}/{collectionId}/products/{productSlug}/{productId}', 'PagesController@showProduct');
    Route::get('/news', 'PagesController@showNews');
    Route::get('/about', 'PagesController@showAbout');
    Route::get('/contact-us', 'PagesController@showContactUs');
    Route::get('/shops', 'PagesController@showShops');

    Route::group(['namespace' => 'Admin', 'as' => 'admin.', 'prefix' => 'admin', 'middleware' => ['auth']], function () {

        Route::get('/', 'HomeController@index')->name('mainpage');
        Route::get('/up', 'HomeController@up');
        Route::get('/down', 'HomeController@down');

        Route::group(['as' => 'users.', 'prefix' => 'users'], function () {

            Route::get('/', 'UserController@index')->name('index');
            Route::get('/create', 'UserController@create')->name('create');
            Route::get('/{userId}/activate', 'UserController@activate');
            Route::get('/{user}/delete', 'UserController@delete');
            Route::post('/store', 'UserController@store');
        });

        Route::group(['as' => 'collections.', 'prefix' => 'collections'], function () {

            Route::get('/', 'CollectionController@index')->name('index');
            Route::get('/create', 'CollectionController@create')->name('create');
            Route::post('/store', 'CollectionController@store');
            Route::get('/{collection}', 'CollectionController@show');
            Route::get('/{collection}/passive', 'CollectionController@passive');
            Route::get('/{collection}/activate', 'CollectionController@activate');
            Route::get('/{collection}/delete', 'CollectionController@delete');
            Route::get('/{collection}/edit', 'CollectionController@edit')->name('edit');
            Route::post('/{collection}/update', 'CollectionController@update');
        });

        Route::group(['as' => 'products.', 'prefix' => 'products'], function () {

            Route::get('/', 'ProductController@index')->name('index');
            Route::get('/create', 'ProductController@create')->name('create');
            Route::get('/{product}', 'ProductController@show')->name('show');
            Route::get('/{product}/edit', 'ProductController@edit')->name('edit');
            Route::get('/{product}/delete', 'ProductController@delete');
            Route::post('/store', 'ProductController@store');
            Route::post('/{product}/update', 'ProductController@update');
            Route::get('/{product}/passive', 'ProductController@passive');
            Route::get('/{product}/activate', 'ProductController@activate');
        });

        Route::group(['as' => 'news.', 'prefix' => 'news'], function () {

            Route::get('/', 'NewsController@index')->name('index');
            Route::get('/create', 'NewsController@create')->name('create');
            Route::post('/store', 'NewsController@store');
            Route::get('/{news}/edit', 'NewsController@edit')->name('edit');
            Route::get('/{news}/publish', 'NewsController@publish');
            Route::get('/{news}/unpublish', 'NewsController@unpublish');
            Route::get('/{news}/delete', 'NewsController@delete');
            Route::post('/{news}/update', 'NewsController@update');
        });

        Route::get('/subscriptions', 'SubscriptionController@index')->name('subscriptions.index');

    });

});


