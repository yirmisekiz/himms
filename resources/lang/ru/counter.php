<?php

return [
    'product_count'    => '2190',
    'product'          => 'товар',
    'customer_count'   => '4152',
    'customer'         => 'покупатель',
    'experience_count' => '26',
    'experience'       => 'лет опыта',
    'shop_count'       => '2',
    'shop'             => 'магазины'
];