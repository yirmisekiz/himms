<?php

return [
    'write_us'        => 'Напишите нам',
    'pick_subject'    => 'Выберите тему',
    'pick_default'    => 'Выбрать',
    'pick_suggestion' => 'предложение',
    'pick_complain'   => 'Пожаловаться',
    'pick_request'    => 'запрос',
    'email'           => 'Эл. почта',
    'name'            => 'название',
    'lname'           => 'фамилия',
    'message'         => 'сообщение',
    'send'            => 'Отправить',
    'shop'            => 'хранить',
    'workshop'        => 'завод',
    'form_success'        => 'Ваше сообщение дошло до нас, мы свяжемся с вами как можно скорее.',
];