<?php

return [
    'inspect_collection' => 'обзор коллекции',
    'mail_label' => 'подписываться',
    'mail_placeholder' => 'Ваш адрес электронной почты',
    'mail_send' => 'добавлять',
    'follow_us' => 'Подписывайтесь на нас',
    'read_more' => 'прочитайте больше',
    'mail_send_success' => 'Ваше письмо сохранено',
    'not_found' => 'Страница не найдена',
    'girl_collection'        => 'Коллекция девушек',
    'boy_collection'         => 'Детская коллекция',
    'fall_winter_collection' => 'Коллекция осенней зимы',
    'outlet_collection'      => 'выход',
];
