<?php

return [
    'main_page'           => 'главная страница',
    'about'               => 'насчет нас',
    'shops'               => 'магазины',
    'news'                => 'Новости',
    'collection'          => 'коллекция',
    'collection_discount' => 'выход',
    'contact_us'          => 'контакт',
];