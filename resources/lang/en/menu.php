<?php

return [
    'main_page'           => 'Home Page',
    'about'               => 'About Us',
    'shops'               => 'Stores',
    'news'                => 'News',
    'collection'          => 'Collection',
    'collection_discount' => 'Outlet',
    'contact_us'          => 'Contact Us',
];