<?php

return [
    'filter_age' => 'Age',
    'filter_age_1' => '1-6 yr',
    'filter_age_2' => '4-8 yr',
    'filter_age_3' => '7-12 yr',
    'filter_age_4' => '10-15 yr',
    'filter_color' => 'Color',
    'filter_category' => 'Category',
];