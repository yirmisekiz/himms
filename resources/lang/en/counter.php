<?php

return [
    'product_count'    => '2190',
    'product'          => 'Product',
    'customer_count'   => '4152',
    'customer'         => 'Customer',
    'experience_count' => '26',
    'experience'       => 'Years of Experience',
    'shop_count'       => '2',
    'shop'             => 'Store'
];