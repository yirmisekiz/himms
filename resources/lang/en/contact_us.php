<?php

return [
    'write_us'        => 'Reach Us',
    'pick_subject'    => 'Subject',
    'pick_default'    => 'Select',
    'pick_suggestion' => 'Suggestion',
    'pick_complain'   => 'Complaint',
    'pick_request'    => 'Request',
    'email'           => 'E-mail',
    'name'            => 'Name',
    'lname'           => 'Surname',
    'message'         => 'Message',
    'send'            => 'Send',
    'shop'            => 'Store',
    'workshop'        => 'Factory',
    'form_success'        => 'Your message has reached us, we will get back to you as soon as possible.',
];