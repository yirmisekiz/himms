<?php

return [
    'inspect_collection' => 'Show',
    'mail_label' => 'Subscribe',
    'mail_placeholder' => 'E-mail Address',
    'mail_send' => 'Send',
    'follow_us' => 'Follow Us',
    'read_more' => 'Read More',
    'mail_send_success' => 'Your email has been saved.',
    'not_found' => 'Page Not Found',
    'girl_collection'        => 'Girls Collection',
    'boy_collection'         => 'Boys Collection',
    'fall_winter_collection' => 'Fall Winter Collection',
    'outlet_collection'      => 'Outlet',
];
