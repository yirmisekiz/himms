<?php

return [
    'inspect_collection'     => 'Koleksiyonu İncele',
    'mail_label'             => 'Yeniliklerimizden Haberdar Olun',
    'mail_placeholder'       => 'E-Posta Adresiniz',
    'mail_send'              => 'EKLE',
    'mail_send_success'      => 'E-posta adresiniz kaydedildi.',
    'follow_us'              => 'Bizi Takip Edin',
    'read_more'              => 'Devamını Oku',
    'not_found'              => 'Sayfa bulunamadı',
    'girl_collection'        => 'Kız Çocuk Koleksiyonu',
    'boy_collection'         => 'Erkek Çocuk Koleksiyonu',
    'fall_winter_collection' => 'Sonbahar Kış Koleksiyonu',
    'outlet_collection'      => 'Outlet',
];
