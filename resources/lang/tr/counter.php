<?php

return [
    'product_count'    => '2190',
    'product'          => 'ÜRÜN',
    'customer_count'   => '4152',
    'customer'         => 'MÜŞTERİ',
    'experience_count' => '26',
    'experience'       => 'YILLIK TECRÜBE',
    'shop_count'       => '2',
    'shop'             => 'MAĞAZA'
];