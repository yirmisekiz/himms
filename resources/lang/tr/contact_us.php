<?php

return [
    'write_us'        => 'Bize Yazın',
    'pick_subject'    => 'Konu Seçiniz',
    'pick_default'    => 'Seçiniz',
    'pick_suggestion' => 'Öneri',
    'pick_complain'   => 'Şikayet',
    'pick_request'    => 'İstek',
    'email'           => 'E-posta',
    'name'            => 'Ad',
    'lname'           => 'Soyad',
    'message'         => 'Mesajınız',
    'send'            => 'Gönder',
    'shop'            => 'Mağaza',
    'workshop'        => 'Fabrika',
    'form_success'        => 'Mesajınız bize ulaştı, size en kısa zamanda geri döneceğiz.',
];