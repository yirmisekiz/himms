<?php

return [
    'filter_age'      => 'Yaş Aralığı',
    'filter_age_1'    => '1-6 Yaş',
    'filter_age_2'    => '4-8 Yaş',
    'filter_age_3'    => '7-12 Yaş',
    'filter_age_4'    => '10-15 Yaş',
    'filter_color'    => 'Renk Seçenekleri',
    'filter_category' => 'Ürün Kategori',
];