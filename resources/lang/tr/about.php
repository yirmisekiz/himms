<?php

return [
    'title' => '“Petek Tekstil olarak ticari hayatımıza 1992 yılında İstanbul Eminönü semtinde toptan satış ile başladık. 2001 yılında yeniden yapılanmaya giderek Himm’s markasıyla 06 Aydan 14 yaşa kadar Tiyneç(teenager) , Hotkütür (Haute Couture ), ve günlük kıyafetler ile kız erkek çocuklarına kaban, manto, elbise, etek, buluz, pantol, ceket, yelek ve tüm giyim kıyafetleriyle abiye kıyafetler olmak üzere alışılmışın dışında farklı kıyafetler üretip pazarladık. Himm’s kısa sürede uzun yollar katedip Türkiye’de aranılan bir marka olmuştur.”',
    'description_1' => "Himm's Kids İstanbul Merter’de 3000 m2 kapalı alanda tasarım, planlama, üretim, ar-ge ve lojistik faaliyetlerini sürdürdüğü fabrika binası ayrıca yine Merter’de yer alan 280 m2 ve Yeşildirek’te bulunan 100 m2 Showroomlarında yurtiçi ve yurtdışına toptan ürün satışı yapmaktadır. Himm’s 2012 yılında mağazacılık ve bayilik sistemine geçerek geniş ürün yelpazesi ile yurtiçi ve yurtdışında ürünlerini pazarlayacak satacak seçkin partner bayiler aranmaktadır.",
    'description_2' => "Himm's Türkiye’nin 600 noktasında ve yurtdışında başta A.B.D. olmak üzere Rusya , Almanya, Fransa, Hollanda, İran , Irak, Suriye, Libya, Ürdün , Suudi Arabistan , Katar, Fas, Tunus, Cezayir, Kuveyt olmak üzere Avrupa ve Asya’nın birçok ülkesine ürün pazarlamaktadır.",
    'description_3' => "Amacımız Himm’s markasını Dünya’nın tanıdığı bir marka olarak ileri yıllara taşımaktır.",
    'description_4' => "Vizyonumuz: Hotkütür (Haute Couture ), Tiyneç (teenager) ve günlük kıyafetler olarak tasarlanıp üretilen farklı tarz ve modellerle bugünün çocukları gelecekte ülkenin teminatı olan çocuklarla bugunden farklı kıyafetlerle farklılıklarını hissettirmektir.",
    'description_5' => "Misyonumuz: Türkiye ‘de daralan tekstil sektöründe yeni ve farklı ürünlerle yeni istidihdamlar sağlayıp ihracatımızı artırıp Türkiye’ye ve Türkiye ekonomisine döviz girdileriyle katkıda bulunmaktır."

];