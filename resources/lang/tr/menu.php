<?php

return [
    'main_page'           => 'Ana Sayfa',
    'about'               => 'Hakkımızda',
    'shops'               => 'Mağazalar',
    'news'                => 'Haberler',
    'collection'          => 'Koleksiyon',
    'collection_discount' => 'Outlet',
    'contact_us'          => 'Bize Ulaşın',
];