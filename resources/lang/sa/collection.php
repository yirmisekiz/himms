<?php

return [
    'filter_age' => 'عمر',
    'filter_age_1' => '1-6 عمر',
    'filter_age_2' => '4-8 عمر',
    'filter_age_3' => '7-12 عمر',
    'filter_age_4' => '10-15 عمر',
    'filter_color' => 'اللون',
    'filter_category' => 'الفئة',
];