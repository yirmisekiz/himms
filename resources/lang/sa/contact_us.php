<?php

return [
    'write_us'        => 'اتصل بنا',
    'pick_subject'    => 'موضوع',
    'pick_default'    => 'تحديد',
    'pick_suggestion' => 'اقتراح',
    'pick_complain'   => 'شكوى',
    'pick_request'    => 'طلب',
    'email'           => 'البريد الإلكتروني',
    'name'            => 'اسم',
    'lname'           => 'لقب',
    'message'         => 'رسالة',
    'send'            => 'إرسال',
    'shop'            => 'متجر',
    'workshop'        => 'مصنع',
    'form_success'        => 'وصلت رسالتك إلينا ، وسنوافيك بالرد في أقرب وقت ممكن',
];