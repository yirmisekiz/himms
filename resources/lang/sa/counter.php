<?php

return [
    'product_count'    => '2190',
    'product'          => 'المنتج',
    'customer_count'   => '4152',
    'customer'         => 'زبون',
    'experience_count' => '26',
    'experience'       => ' سنوات',
    'shop_count'       => '2',
    'shop'             => 'مخازن'
];