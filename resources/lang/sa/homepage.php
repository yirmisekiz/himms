<?php

return [
    'inspect_collection'     => 'استعراض مجموعة',
    'mail_label'             => 'الاشتراك',
    'mail_placeholder'       => 'عنوان بريد الكتروني',
    'mail_send'              => 'إضافة',
    'follow_us'              => 'تابعنا',
    'read_more'              => 'قراءة المزيد',
    'mail_send_success'      => 'تم حفظ بريدك الإلكتروني',
    'not_found'              => 'الصفحة غير موجودة',
    'girl_collection'        => 'مجموعة البنات',
    'boy_collection'         => 'مجموعة الصبي',
    'fall_winter_collection' => 'مجموعة خريف وشتاء',
    'outlet_collection'      => 'منفذ',
];
