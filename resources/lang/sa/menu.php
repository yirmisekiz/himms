<?php

return [
    'main_page'           => 'الصفحة الرئيسية',
    'about'               => 'معلومات عنا',
    'shops'               => 'مخازن',
    'news'                => 'أخبار',
    'collection'          => 'مجموعة',
    'collection_discount' => 'مخرج',
    'contact_us'          => 'اتصل بنا',
];