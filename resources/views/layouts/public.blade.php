<!DOCTYPE html>
<html class="lang-{{session('locale')}}" lang="{{session('locale')}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>{{isset($pageTitle) ?  "Himm's Kids - " . $pageTitle : "Himm's Kids" }}</title>
    <link rel="shortcut icon" href="/img/fav.png">

    <meta property="og:url" content="https://www.himms.com.tr"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Himm's KidsWear"/>
    <meta property="og:description" content="“We started our business life by wholesale trading in Istanbul, Eminönü in 1992 as Petek Tekstil under the brand of Himm's Kids. Undergoing restructuring process in 2001, we manufactured and marketed extraordinary wears including teenager clothes for the age range of 6 months to 14 years, Haute Couture and casual dresses as well as coat, dress, skirt, blouse, trousers, jacket, waist for girls and boys and all wearing components and evening dresses. Himm’s showed significant progress in a short time and has become a brand seeked for in Turkey.”"/>

    <!-- build:css -->
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/theme.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="/css/owl.carousel.css">
    <link rel="stylesheet" href="/css/all.css">
    <link rel="stylesheet" href="/css/xzoom.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/aos.css">
    <!-- endbuild -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129818796-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-129818796-1');
    </script>

</head>

<body>

<!-- Modal -->
<div class="modal" id="mainMenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="centrize">
        <div class="v-center">
            <div class="container mainMenu">
                <a class="close" href="#" data-dismiss="modal"> <img src="/img/close.svg"> </a>

                <ul class="list-unstyled text-right">
                    <li>
                        <a href="/{{session('locale')}}/about">{{__('menu.about')}}</a>
                    </li>

                    <li>
                        <a class="pink font-bold" data-toggle="collapse" href="#sub-link" role="button" aria-expanded="false"
                           aria-controls="multiCollapseExample1"><b>{{__('menu.collection')}}</b></a>

                        <div class="collapse multi-collapse sub-link" id="sub-link">
                            <ul class="list-unstyled">
                                @if(session('locale') === 'tr')
                                    <li>
                                        <a href="/{{session('locale')}}/collections/kiz-cocuk-koleksiyonu/4">{{__('homepage.girl_collection')}}</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="/{{session('locale')}}/collections/girls-fashion-collection/4">{{__('homepage.girl_collection')}}</a>
                                    </li>
                                @endif
                                @if(session('locale') === 'tr')
                                    <li>
                                        <a href="/{{session('locale')}}/collections/erkek-cocuk-koleksiyonu/5">{{__('homepage.boy_collection')}}</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="/{{session('locale')}}/collections/boy-fashion-collection/5">{{__('homepage.boy_collection')}}</a>
                                    </li>
                                @endif
                                @if(session('locale') === 'tr')
                                    <li>
                                        <a href="/{{session('locale')}}/collections/sonbahar-kis/6">{{__('homepage.fall_winter_collection')}}</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="/{{session('locale')}}/collections/fall-winter/6">{{__('homepage.fall_winter_collection')}}</a>
                                    </li>
                                @endif
                            </ul>
                        </div>

                    </li>

                    <li>
                        @if(str_replace([env('APP_URL'),session('locale')],['',''],url()->current()) === '//collection')
                            <a style="font-weight: 600" class="nav-link" href="/collection">{{__('menu.collection_discount')}}</a>
                        @else
                            <a style="font-weight: 600" class="nav-link pink font-bold" href="/collection">{{__('menu.collection_discount')}}</a>
                        @endif
                    </li>

                    <li>
                        <a href="/{{session('locale')}}/shops">{{__('menu.shops')}}</a>
                    </li>

                    <li>
                        <a href="/{{session('locale')}}/news">{{__('menu.news')}}</a>
                    </li>

                    <li>
                        <a href="/{{session('locale')}}/contact-us">{{__('menu.contact_us')}}</a>
                    </li>

                </ul>

                <ul class="list-unstyled mx-auto text-center lang">
                    <li class="list-inline-item">
                        <a href="{{   str_replace(['/tr','/en','/sa','/ru'],['/tr','/tr','/tr','/tr'],url()->full()) }}">
                            <img src="/img/lang-tr.svg"> </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{   str_replace(['/tr','/en','/sa','/ru'],['/en','/en','/en','/en'],url()->full()) }}">
                            <img src="/img/lang-en.svg"> </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{   str_replace(['/tr','/en','/sa','/ru'],['/sa','/sa','/sa','/sa'],url()->full()) }}">
                            <img src="/img/lang-sa.svg"> </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{   str_replace(['/tr','/en','/sa','/ru'],['/ru','/ru','/ru','/ru'],url()->full()) }}">
                            <img src="/img/lang-ru.svg"> </a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</div>

<!-- Collapse Menu-->
<div class="collapse menu" id="collapseMenu">
    <div class="row">

        <div class="col-md-4 girl">
            @if(session('locale') === 'tr')
                <a href="/{{session('locale')}}/collections/kiz-cocuk-koleksiyonu/4">
                    @else
                        <a href="/{{session('locale')}}/collections/girls-fashion-collection/4">
                            @endif
                            <div class="card">
                                <img class="card-img-top img-fluid" src="/img/slider-ust-kiz-koleksiyon-modified.jpg">
                                <div class="card-footer">
                                    <b>{{__('homepage.girl_collection')}}</b>
                                </div>

                            </div>
                        </a>

        </div>

        <div class="col-md-4 boy">

            @if(session('locale') === 'tr')
                <a href="/{{session('locale')}}/collections/erkek-cocuk-koleksiyonu/5">
                    @else
                        <a href="/{{session('locale')}}/collections/boy-fashion-collection/5">
                            @endif
                            <div class="card">

                                <img class="card-img-top img-fluid" src="/img/koleksiyon-üst-kısım-erkek-çocuk-modified.jpg">
                                <div class="card-footer">
                                    <b>{{__('homepage.boy_collection')}}</b>
                                </div>

                            </div>
                        </a>
        </div>

        <div class="col-md-4 season">
            @if(session('locale') === 'tr')
                <a href="/{{session('locale')}}/collections/sonbahar-kis/6">
                    @else
                        <a href="/{{session('locale')}}/collections/fall-winter/6">
                            @endif
                            <div class="card">

                                <img class="card-img-top img-fluid" src="/img/season.png">
                                <div class="card-footer">
                                    <b>{{__('homepage.fall_winter_collection')}}</b>
                                </div>

                            </div>
                        </a>
        </div>

    </div>
</div>

<!-- Top-->
<div class="top">
    <div class="container">
        <div class="row">

            <div class="col-md-6 mail">
                <a href="mailto:info@himms.com.tr">info@himms.com.tr</a>
            </div>

            <div class="col-md-6 phone">
                <a href="tel:02126779842">0212 677 98 42</a>
            </div>

        </div>
    </div>
</div>

<!-- Nav -->
<nav class="navbar navbar-expand-lg navbar-light">

    <div class="container">
        <a class="navbar-brand" href="/"> <img src="/img/logo@3x.png"> </a>
        <button class="navbar-toggler" type="button" data-toggle="modal" data-target="#mainMenu">
            {{--<span class="navbar-toggler-icon"></span>--}}
            <img src="/img/hamburger.svg">
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav mx-auto main-menu">

                <li class="nav-item">
                    @if(str_replace([env('APP_URL'),session('locale')],['',''],url()->current()) === '/')
                        <a style="font-weight: 600" class="nav-link" href="/">{{__('menu.main_page')}}</a>
                    @else
                        <a class="nav-link" href="/">{{__('menu.main_page')}}</a>
                    @endif

                </li>

                <li class="nav-item">
                    @if(str_replace([env('APP_URL'),session('locale')],['',''],url()->current()) === '//about')
                        <a style="font-weight: 600" class="nav-link" href="/{{session('locale')}}/about">{{__('menu.about')}}
                            <span class="sr-only">(current)</span></a>
                    @else
                        <a class="nav-link" href="/{{session('locale')}}/about">{{__('menu.about')}}
                            <span class="sr-only">(current)</span></a>
                    @endif

                </li>

                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#collapseMenu" role="button" aria-expanded="false" aria-controls="collapseMenu"><b class="pink">{{__('menu.collection')}}</b></a>
                </li>

                <li class="nav-item">
                    @if(str_replace([env('APP_URL'),session('locale')],['',''],url()->current()) === '//collection')
                        <a style="font-weight: 600" class="nav-link" href="/{{session('locale')}}/collections/outlet/7">{{__('menu.collection_discount')}}</a>
                    @else
                        <a style="font-weight: 600" class="nav-link pink font-bold" href="/{{session('locale')}}/collections/outlet/7">{{__('menu.collection_discount')}}</a>
                    @endif

                </li>

                <li class="nav-item">
                    @if(str_replace([env('APP_URL'),session('locale')],['',''],url()->current()) === '//shops')
                        <a style="font-weight: 600" class="nav-link" href="/{{session('locale')}}/shops">{{__('menu.shops')}}</a>
                    @else
                        <a class="nav-link" href="/{{session('locale')}}/shops">{{__('menu.shops')}}</a>
                    @endif
                </li>

                <li class="nav-item">
                    @if(str_replace([env('APP_URL'),session('locale')],['',''],url()->current()) === '//news')
                        <a style="font-weight: 600" class="nav-link" href="/{{session('locale')}}/news">{{__('menu.news')}}</a>
                    @else
                        <a class="nav-link" href="/{{session('locale')}}/news">{{__('menu.news')}}</a>
                    @endif
                </li>

                <li class="nav-item">
                    @if(str_replace([env('APP_URL'),session('locale')],['',''],url()->current()) === '//contact-us')
                        <a style="font-weight: 600" class="nav-link" href="/contact-us">{{__('menu.contact_us')}}</a>
                    @else
                        <a class="nav-link" href="/contact-us">{{__('menu.contact_us')}}</a>
                    @endif
                </li>

            </ul>

            <ul class="navbar-nav language">
                <li class="nav-item lang dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if (App::isLocale('en'))
                            <img src="/img/lang-en.svg">
                        @elseif(App::isLocale('sa'))
                            <img src="/img/lang-sa.svg">
                        @elseif(App::isLocale('ru'))
                            <img src="/img/lang-ru.svg">
                        @else
                            <img src="/img/lang-tr.svg">
                        @endif
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item lang" href="{{   str_replace(['/tr','/en','/sa','/ru'],['/tr','/tr','/tr','/tr'],url()->full()) }}"><img src="/img/lang-tr.svg"></a>
                        <a class="dropdown-item lang" href="{{   str_replace(['/tr','/en','/sa','/ru'],['/en','/en','/en','/en'],url()->full()) }}"><img src="/img/lang-en.svg"></a>
                        <a class="dropdown-item lang" href="{{   str_replace(['/tr','/en','/sa','/ru'],['/sa','/sa','/sa','/sa'],url()->full()) }}"><img src="/img/lang-sa.svg"></a>
                        <a class="dropdown-item lang" href="{{   str_replace(['/tr','/en','/sa','/ru'],['/ru','/ru','/ru','/ru'],url()->full()) }}"><img src="/img/lang-ru.svg"></a>
                    </div>
                </li>
            </ul>

        </div>
    </div>

</nav>

@yield('content')

<!-- E-Bulletin-->
<div class="e-bulletin">
    <div class="container">
        <div class="col-lg-4 col-md-6 offset-md-3 offset-lg-4">
            <form id="js-subscribe-form" action="/{{session('locale')}}/addSubscription" method="POST">
                @csrf
                <div class="form-group ml-2">
                    <button type="submit" class="btn btn-link">{{__('homepage.mail_send')}}</button>
                    <label>{{__('homepage.mail_label')}}</label>
                    <input name="email" type="email" required placeholder="{{__('homepage.mail_placeholder')}}" class="form-control form-input">
                </div>
            </form>
            <div id="js-subscribe-success" style="margin-top: 10px">
                <span>{{__('homepage.mail_send_success')}}</span>
            </div>
        </div>
    </div>
</div>

<!-- Footer-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mx-auto text-center">

                <h6>{{__('homepage.follow_us')}}</h6>
                <ul class="list-unstyled social">

                    <li class="list-inline-item">
                        <a href="https://facebook.com/himmskids/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    </li>

                    <li class="list-inline-item">
                        <a href="https://twitter.com/himms_kids" target="_blank"><i class="fab fa-twitter"></i></a>
                    </li>

                </ul>

            </div>

            <div class="col-md-6 copy">
                <span>Copyright © 2018 <b>Himm's Kidswear</b></span>
            </div>

            <div class="col-md-6 design">
                <a style="color:white" href="http://hepta.com.tr" target="_blank">By Hepta</a>
            </div>
        </div>
    </div>
</footer>

<!-- build:js -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script src="/js/main.js"></script>
<script src="/js/theme.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/setup.js"></script>
<script src="/js/foundation.min.js"></script>
<script src="/js/xzoom.min.js"></script>
<script src="/js/aos.js"></script>
<script src="/js/jquery.counterup.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        $('#js-subscribe-success').hide();
        $('#js-subscribe-success-2').hide();

        $('#js-subscribe-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: $('#js-subscribe-form').attr('action'),
                data: $('#js-subscribe-form').serialize(),
                success: function () {
                    $('#js-subscribe-success').show();
                    $('#js-subscribe-form')[0].reset();
                }
            });
            e.preventDefault();
            return false;
        })
        $('#js-subscribe-form-2').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: $('#js-subscribe-form-2').attr('action'),
                data: $('#js-subscribe-form-2').serialize(),
                success: function () {
                    $('#js-subscribe-success-2').show();
                    $('#js-subscribe-form-2')[0].reset();
                }
            });
            e.preventDefault();
            return false;
        })
    });


    function count(id) {
        if ($('#' + id).length < 1) {
            return;
        }
        var numAnim = new CountUp(id, 1, $('#' + id).text(), 0, 10);
        if (!numAnim.error) {
            numAnim.start();
        } else {
            console.error(numAnim.error);
        }
    }

    count("js-count-1");
    count("js-count-2");
    count("js-count-3");
    count("js-count-4");

    AOS.init({
        duration: 1000
    });

    $('.owl-carousel-2').owlCarousel({
        rewind: true,
        margin: 10,
        responsiveClass: true,
        autoplay: true,
        autoplayTimeout: 6000,
        responsive: {
            0: {
                items: 3,
                nav: false,
                loop: true
            },
            600: {
                items: 4,
                nav: false,
                loop: true
            },
            1000: {
                items: 8,
                nav: false,
                loop: true
            }
        }
    });

</script>
<!-- Google Analytics -->

@section('scripts')

@show

</body>

</html>