<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Himms </title>
    <link rel="stylesheet" href="{{asset('css/admin/vendor.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/admin/app.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/admin/plugins/datapicker/datepicker3.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/admin/plugins/select2/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/admin/plugins/select2/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/admin/plugins/sweetalert/sweetalert.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/admin/plugins/dropzone/basic.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/admin/plugins/dropzone/dropzone.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/admin/plugins/dataTables/datatables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/admin/style.css')}}"/>

</head>
<body>
@if(!empty(session()->get('success')))
    <input data-title='Başarılı' data-type="success" hidden id="js-swal-msg" value="{{ session()->get('success') }}">
@elseif(!empty(session()->get('error')))
    <input data-title='Hatalı' data-type="error" hidden id="js-swal-msg" value="{{ session()->get('error') }}">
@endif

<!-- Wrapper-->
<div id="wrapper">

@if(auth()->user())
    <!-- Navigation -->
    @include('admin.partials.navigation')
@endif
<!-- Page wraper -->
    <div id="page-wrapper" class="gray-bg">

    @if(auth()->user())
        <!-- Page wrapper -->
            @include('admin.partials.topnavbar')
        @endif

        <div class="wrapper wrapper-content animated fadeIn">
            <!-- Main view  -->
            @yield('content')

        </div>

        <!-- Footer -->
        @include('admin.partials.footer')
    </div>
    <!-- End page wrapper-->

</div>
<!-- End wrapper-->

<script src="{{ asset('/js/admin/js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/admin/js/plugins/dataTables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/admin/js/plugins/datapicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/admin/js/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/admin/js/plugins/sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/admin/js/plugins/dropzone/dropzone.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/admin/js/theme.js') }}" type="text/javascript"></script>

@section('scripts')

@show

</body>
</html>
