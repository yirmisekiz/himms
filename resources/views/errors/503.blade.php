<style rel="stylesheet" type="text/css">
    button{
        display:none!important;
    }
</style>

@extends('errors::illustrated-layout')

@section('code', '')
@section('title', __('Service Unavailable'))

@section('image')
    <div style="background-image: url('/img/logo@3x.png');" class="absolute pin bg-no-repeat md:bg-left lg:bg-center"></div>
@endsection

@section('message')
    Sizlere daha iyi hizmet verebilmek icin yenileniyoruz. En kısa sürede birlikteyiz!
    <br>
    <address>
        E-posta:
        <br>
        <a href="mailto:info@himms.com.tr">info@himms.com.tr</a>
    </address>
    <br>
    <br>
    <address>
        Tel:
        <br>
        <a href="tel:02126779842">+90 212 677 9842</a><br>
    </address>
@endsection


