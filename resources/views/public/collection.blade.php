@extends('layouts.public')

@section('content')
    <!-- Sub Header-->
    <div class="sub-header" style="height: 200px!important;">
        <div class="container">
            <div class="col- offset-lg-1">
                <ul class="list-unstyled">
                    <li><h4>Himm's</h4></li>
                    <li><h5 class="pink">{{$collection->$name}}</h5></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Catalog-->
    <div class="content mt-4">

        <div class="container">
            <div class="row">

                <div class="col-md-3 filter">

                    <div class="card girl">
                        <img class="card-img-top" src="{{$collection->image}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$collection->$name}}</h5>
                            <a class="btn btn-primary" data-toggle="collapse" href="#girl" role="button" aria-expanded="false" aria-controls="girl"> </a>
                        </div>

                        <div class="card-footer">
                            <div class="collapse show" id="girl">
                                <div class="card ">
                                    <ul class="list-unstyled age">

                                        <li class="mb-2"><span>{{__('collection.filter_age')}}</span></li>

                                        @foreach($availableAges as $ageFilter)
                                            <li>
                                                <input class="checkboxs js-age-filters" id="js-age-filter-{{$ageFilter->id}}" type="checkbox" value="{{$ageFilter->id}}" checked>
                                                <label for="js-age-filter-{{$ageFilter->id}}">{{$ageFilter->$name}}</label>
                                            </li>
                                        @endforeach

                                    </ul>

                                    <hr class="col-">

                                    <ul class="list-unstyled age">

                                        <li class="mb-2"><span>{{__('collection.filter_category')}}</span></li>

                                        @foreach($availableCategories as $category)
                                            <li>
                                                <input class="checkboxs js-category-filters" id="js-category-filter-{{$category->id}}" type="checkbox" value="{{$category->id}}" checked>
                                                <label for="js-category-filter-{{$category->id}}">{{$category->$name}}</label>
                                            </li>
                                        @endforeach

                                    </ul>

                                    <hr class="col-">

                                    <ul class="list-unstyled color">
                                        <li class="mb-2"><span>{{__('collection.filter_color')}}</span></li>
                                        @foreach($colors as $color)
                                            <li>
                                                <input class="checkboxs js-color-filters" id="color-{{$color->id}}" type="checkbox" value="{{$color->id}}" checked>
                                                <label for="color-{{$color->id}}">
                                                    <img style="width: 25px;height: 25px;background: {{$color->hex_code}}">
                                                </label>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                    @foreach($otherCollections as $otherCollection)

                        @if(session('locale') === 'tr')
                            <a href="/{{session('locale')}}/collections/{{$otherCollection->slug_tr}}/{{$otherCollection->id}}">
                        @else
                            <a href="/{{session('locale')}}/collections/{{$otherCollection->slug_en}}/{{$otherCollection->id}}">
                        @endif

                            <div class="card boy mt-3">
                                <img class="card-img-top" src="{{$otherCollection->image}}" alt="{{$otherCollection->$name}}">
                                <div class="card-body">
                                    <h5 class="card-title">{{$otherCollection->$name}}</h5>
                                </div>
                                <div class="card-footer"></div>
                            </div>
                        </a>
                    @endforeach

                </div>

                <div class="col-md-9 catalog">

                    <div class="container">
                        <div class="row">

                            @foreach($collection->products as $product)


                                @if($product->firstImage && $product->is_passive !== 1)
                                    <div class="col-md-3 mb-4 js-product-items" data-aos="fade-up" data-filter-age-option-id="{{$product->age_id}}" data-filter-color-options-id="{{ join(',', $product->colors->pluck('id')->toArray() ) }}">
                                        <a href="/{{session('locale')}}/collections/{{$collection->$slugCollection}}/{{$collection->id}}/products/{{$product->slug_tr}}/{{$product->id}}">
                                            <div class="card">
                                                <div class="overlay">
                                                    <i class="fas fa-search mx-auto"></i>
                                                </div>
                                                <img class="card-img-top" src="{{$product->firstImage->path}}" alt="{{$product->code}} - {{$product->name}} Fotoğrafı">
                                            </div>
                                            <span class="code">{{$product->code}} - {{$product->name}}</span> </a>
                                    </div>
                                @endif

                            @endforeach

                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <div class="mobile-menu d-lg-none d-md-none d-sm-block">
        <div class="container">
            <ul class="list-unstyled">

                <li class="list-inline-item">
                    <a href="#"> <img src="/img/girl.svg"> <br> Kız Çocuk </a>
                </li>

                <li class="list-inline-item">
                    <a href="#"> <img src="/img/boy.svg"> <br> Erkek Çocuk </a>
                </li>

                <li class="list-inline-item">
                    <a href="#"> <img src="/img/leaf.svg"> <br> Sonbahar-Kış </a>
                </li>
            </ul>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            var ageFilter = '.js-age-filters',
                productItems = '.js-product-items',
                colorFilter = '.js-color-filters',
                categoryFilter = '.js-category-filters',
                colorDataAttribute = 'filter-color-options-id',
                ageDataAttribute = 'filter-age-option-id',
                categoryDataAttribute = 'filter-category-option-id';

            function filter(filterName, filterDataAttribute) {
                var selected = [];
                $(filterName + ':checked').each(function () {
                    selected.push($(this).val());
                });
                if (selected.length === 0) {
                    $(productItems).hide();
                }
                $.each($(productItems), function (ind, elem) {
                    if (containsAny(selected, $(elem).data(filterDataAttribute).toString().split(','))) {
                        $(elem).show();
                    } else {
                        $(elem).hide();
                    }
                });
                return true;
            }

            function containsAny(source, target) {
                var result = source.filter(function (item) {
                    return target.indexOf(item) > -1
                });
                return (result.length > 0);
            }

            $(colorFilter).on('change', function () {
                filter(colorFilter, colorDataAttribute);
            });

            $(categoryFilter).on('change', function () {
                filter(categoryFilter, categoryDataAttribute);
            });

            $(ageFilter).on('change', function () {
                filter(ageFilter, ageDataAttribute);
            });

        });
    </script>
@endsection
