@extends('layouts.public')

@section('content')

    <!-- Sub Header-->
    <div class="sub-header contact map">
        {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3012.267064677662!2d28.871058615247573!3d40.97563252938228!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cabcba94cdf5cd%3A0x7b3db987ec43b5de!2zSG9jYSBIYXNhbiBTay4sIEJha8SxcmvDtnkvxLBzdGFuYnVs!5e0!3m2!1str!2str!4v1540210003692" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>--}}
        <iframe src="https://www.google.com/maps/d/embed?mid=1NBr18FIUV6urg1eIgyEqg1DBV-MXfKiS" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>

    <div class="content">

        <div class="contact">
            <div class="container">

                <div class="row address">
                    <div class="col-md-4">
                        <h1><i class="fas fa-store"></i></h1>
                        <h5 class="mt-3">Yeşildirek {{__('contact_us.shop')}}</h5>

                        <address class="mt-3">
                            Sururi Mah. Hocahanı Sokak <br>
                            Hamdibey Geçidi Hanı No:21/D<br>
                            Yeşildirek Eminönü - İstanbul/Türkiye<br><br>
                            <b>T:</b> <a href="tel:02125124308">+90 212 512 4308</a><br>
                            <b>F:</b> <a href="tel:02125124307">+90 212 512 4307</a><br>
                        </address>
                    </div>

                    <div class="col-md-4">
                        <h1><i class="fas fa-building"></i></h1>
                        <h5 class="mt-3">{{__('contact_us.workshop')}}</h5>
                        <address class="mt-2">
                            Merter Keresteciler Sitesi  <br>
                            Şimşir Sokak No:13 Kat No: 2-3<br>
                            Merter - İstanbul/Türkiye<br><br>

                            <b>T:</b> <a href="tel:02126779842">+90 212 677 9842</a><br>
                            <b>F:</b> <a href="tel:02126779762">+90 212 677 9762</a><br>
                        </address>
                    </div>

                    <div class="col-md-4">
                        <h1><i class="fas fa-envelope"></i></h1>
                        <h5 class="mt-3">{{__('contact_us.email')}}</h5>
                        <address>
                            <a href="mailto:info@himms.com.tr">info@himms.com.tr</a>
                        </address>
                    </div>

                </div>

                <div class="row contact-form">
                    <div class="col-md-8 offset-lg-2">

                        <h1>{{__('contact_us.write_us')}}</h1>

                        <form id="js-subscribe-form-2" action="/{{session('locale')}}/sendEmail" method="POST">
                            @csrf

                            <div class="row mt-4">

                                <div id="js-subscribe-success-2" class="col-md-12">
                                    <div class="alert alert-success" role="alert">
                                        <i class="fas fa-check"></i> {{__('contact_us.form_success')}}
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{__('contact_us.pick_subject')}}</label>
                                        <select name="reason" class="custom-select">
                                            <option disabled selected>{{__('contact_us.pick_default')}}</option>
                                            <option value="{{__('contact_us.pick_suggestion')}}">{{__('contact_us.pick_suggestion')}}</option>
                                            <option value="{{__('contact_us.pick_complain')}}">{{__('contact_us.pick_complain')}}</option>
                                            <option value="{{__('contact_us.pick_request')}}">{{__('contact_us.pick_request')}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('contact_us.name')}} {{__('contact_us.lname')}}</label>
                                        <input name="name" type="text" class="form-control form-input" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input name="email" type="email" class="form-control form-input">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>{{__('contact_us.message')}}</label>
                                        <textarea name="message" type="text" rows="5" class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-light btn-lg btn-block">
                                        <b>{{__('contact_us.send')}}</b>
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection