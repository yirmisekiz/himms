@extends('layouts.public')

@section('content')

    <!-- Catalog-->
    <div class="content mb-5 product-detail" data-aos="fade-down">

        <div class="container">
            <div class="row">

                <div class="col-lg-6">
                    <div class="xzoom-container">
                        @if(!is_null($product->images->first()) )
                            <img class="xzoom" id="xzoom-default" src="{{$product->images->first()->path}}" xoriginal="{{$product->images->first()->path}}"/>
                        @endif
                        <div class="xzoom-thumbs mt-3">
                            @foreach($product->images as $image)
                                <a href="{{$image->path}}">
                                    <img class="xzoom-gallery" width="80" src="{{$image->path}}" xpreview="{{$image->path}}">
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 product-info">

                    <div class="row">
                        <div class="col-md-10">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{__('menu.collection')}}</a></li>
                                <li class="breadcrumb-item"><a>{{$product->name}}</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{$product->code}}</li>
                            </ol>
                            <ul class="list-unstyled">
                                <li><span class="name">{{$product->name}}</span></li>
                            </ul>
                        </div>

                        <div class="col-md-2">
                            <div class="social" style="display:none">
                                <a href="https://www.facebook.com/sharer.php?u={{$baseURL}}/{{session('locale')}}/collections/{{$collection->$slugCollection}}/{{$collection->id}}/products/{{$product->slug_tr}}/{{$product->id}}&title={{$product->name}}&picture={{$product->images->first() ? getenv('APP_URL') . $product->images->first()->path : '' }}&description={{$product->$description}}" class="btn btn-sm facebook btn-light">
                                    <i class="fab fa-facebook-f"></i> {{__('product.share_fb')}}
                                </a>
                            </div>
                        </div>
                    </div>

                    @if($product->colors->isNotEmpty())
                        <div class="color mt-3">
                        <span class="title">
                            {{__('collection.filter_color')}}
                        </span>
                            <ul class="list-unstyled mt-2">

                                @foreach($product->colors as $color)
                                    <li class="list-inline-item">
                                        <img style="border:18px solid {{$color->hex_code}};border-radius:50%;height:25px;width: 25px;background:{{$color->hex_code}}">
                                    </li>
                                @endforeach

                            </ul>

                        </div>
                    @endif

                    @if(!is_null($product->age))

                        <div class="size mt-5">

                        <span class="title">
                            {{__('collection.filter_age')}}
                        </span>

                            <div class="row">
                                <div class="col-md-4 mt-3">
                                    <a>
                                        <div class="box active">
                                            {{$product->age->$name}}
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="desc mt-4">
                        <span class="title">{{__('product.description_and_usage')}}</span>
                        <p class="d-block mt-2">
                            {{$product->$description}}
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @if($otherProducts->isNotEmpty())
        <div class="content mt-5" data-aos="fade-up">
            <div class="catalog">

                <div class="container">
                    <h5>{{__('product.other')}} {{$collection->$name}} Ürünleri</h5>
                </div>

                <div class="owl-carousel-2 owl-carousel mt-4">

                    @foreach($otherProducts as $otherProduct)
                        <div>
                            <a href="/{{session('locale')}}/collections/{{$collection->slug_tr}}/{{$collection->id}}/products/{{$otherProduct->slug_tr}}/{{$otherProduct->id}}">
                                <div class="card">
                                    <div class="overlay">
                                        <i class="fas fa-search mx-auto"></i>
                                    </div>
                                    @if(!is_null($otherProduct->firstImage))
                                        <img class="card-img-top" src="{{$otherProduct->firstImage->path}}" alt="Card image cap">
                                    @endif
                                </div>
                            </a>
                        </div>
                    @endforeach

                </div>

            </div>
        </div>
    @endif


@endsection
