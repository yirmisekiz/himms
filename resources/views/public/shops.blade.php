@extends('layouts.public')

@section('content')
    <!-- Sub Header-->
    <div class="sub-header shops">
        <div class="container">
            <div class="col- offset-lg-1">
                <ul class="list-unstyled">
                    <li><h4>Himm's</h4></li>
                    <li><h5>{{__('menu.shops')}}</h5></li>
                    <hr>
                </ul>
            </div>
        </div>
    </div>

    <div class="content mt-4">

        <div class="container-fluid">

            <div class="row">
                <div class="col-md-2 p-0 sub-menu">
                    <ul class="list-unstyled">
                        <li>
                            <a href="/{{session('locale')}}/about">
                                {{__('menu.about')}}
                            </a>
                        </li>

                        <li class="active">
                            <a href="/{{session('locale')}}/shops">
                                {{__('menu.shops')}}
                            </a>
                        </li>
                        <li>
                            <a href="/{{session('locale')}}/news">
                                {{__('menu.news')}}
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="col-md-8 offset-lg-1 mt-2">

                    <div class="shops">

                        <div data-aos="fade-up">

                            <h3>Yeşildirek {{__('shops.our_shop')}}</h3>
                            <div class="row mt-4">
                                <div class="col-md-3">
                                    <img class="img-thumbnail" src="/img/yesildirek-magaza.jpeg">
                                </div>

                                <div class="col-md-4 mt-3">
                                    <ul class="list-unstyled">
                                        <li><strong>{{__('shops.address')}}</strong></li>
                                        <li class="mt-2">
                                            <address>
                                                Sururi Mah. Hocahanı Sokak <br>
                                                Hamdibey Geçidi Hanı No:21/D<br>
                                                Yeşildirek Eminönü - İstanbul/Türkiye<br><br>
                                            </address>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-4 mt-3">
                                    <ul class="list-unstyled">
                                        <li><strong>{{__('shops.contact')}}</strong></li>
                                        <li class="mt-2">
                                            <address>
                                                <a href="tel:02125124297">+90 212 512 4297</a><br>
                                                <a href="tel:02125124307">+90 212 512 4307</a><br>
                                            </address>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12 mt-3">
                                    <iframe src="https://www.google.com/maps/d/embed?mid=1ok0sqryqP_uOWGm9F3j5mW3yLyox0324" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>

                            </div>

                        </div>

                        <hr class="col-">

                        <div data-aos="fade-up">

                            <h3>{{__('contact_us.workshop')}}</h3>
                            <div class="row mt-4">
                                <div class="col-md-3">
                                    <img class="img-thumbnail" src="/img/fabrika.jpg">
                                </div>

                                <div class="col-md-4 mt-3">
                                    <ul class="list-unstyled">
                                        <li><strong>{{__('shops.address')}}</strong></li>
                                        <li class="mt-2">
                                            <address>
                                                Merter Keresteciler Sitesi
                                                <br> Selvi Sokak No:16/2<br> Merter/İstanbul/Türkiye<br>
                                            </address>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-4 mt-3">
                                    <ul class="list-unstyled">
                                        <li><strong>{{__('shops.contact')}}</strong></li>
                                        <li class="mt-2">
                                            <address>
                                                <a href="tel:02125124297">+90 212 512 4297</a><br>
                                                <a href="tel:02125124307">+90 212 512 4307</a><br>
                                            </address>
                                        </li>
                                    </ul>
                                </div>

                                <div class="col-md-12 mt-3">
                                    <iframe src="https://www.google.com/maps/d/embed?mid=1H7JsluP0kCuvvPo69O3H382VZWxEGSj8" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection