@extends('layouts.public')

@section('content')

    <!-- Banner -->
    <div class="jumbotron page-hero jumbotron-fluid" data-aos="fade-up">
        <div class="content">
            <div class="owl-carousel">
                <div class="item">
                    <div class="caption">
                        <ul class="list-unstyled">
                            <li><h1>HIMM'S</h1></li>
                            <li><span>{{__('homepage.fall_winter_collection')}}</span></li>
                            <li class="mt-3">
                                @if(session('locale') === 'tr')
                                    <button onclick='window.location.href="/{{session('locale')}}/collections/sonbahar-kis/6"' type="button" class="btn btn-light">{{__('homepage.inspect_collection')}}</button>
                                @else
                                    <button onclick='window.location.href="/{{session('locale')}}/collections/fall-winter/6"' type="button" class="btn btn-light">{{__('homepage.inspect_collection')}}</button>
                                @endif
                            </li>
                        </ul>
                    </div>
                    <img class="img-fluid " src="/img/slider-1-modified.jpg">
                </div>
                <div class="item">
                    <div class="caption ">
                        <ul class="list-unstyled">
                            <li><h1>HIMM'S</h1></li>
                            <li><span>{{__('homepage.girl_collection')}}</span></li>
                            <li class="mt-3">
                                @if(session('locale') === 'tr')
                                    <button onclick='window.location.href="/{{session('locale')}}/collections/kiz-cocuk-koleksiyonu/4"' type="button" class="btn btn-light">{{__('homepage.inspect_collection')}}</button>
                                @else
                                    <button onclick='window.location.href="/{{session('locale')}}/collections/girls-fashion-collection/4"' type="button" class="btn btn-light">{{__('homepage.inspect_collection')}}</button>
                                @endif
                            </li>
                        </ul>
                    </div>
                    <img class="img-fluid " src="/img/slider-2-formatted.jpg">

                </div>
            </div>
        </div>
    </div>

    <!-- Middle Catalog -->
    <div class="middle-catalog mt-4" data-aos="fade-up">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="card girl">
                        <img class="card-img" src="/img/slider-alti-kiz-cocuk-koleksiyon-modified.jpg">
                        <div class="card-img-overlay">
                            <article>
                                <ul class="list-unstyled">
                                    <li><h5>{{__('homepage.girl_collection')}}</h5></li>
                                    <li class="mt-3">
                                        @if(session('locale') === 'tr')
                                            <button onclick='window.location.href="/{{session('locale')}}/collections/kiz-cocuk-koleksiyonu/4"' type="button" class="btn btn-light btn-sm">{{__('homepage.inspect_collection')}}</button>
                                        @else
                                            <button onclick='window.location.href="/{{session('locale')}}/collections/girls-fashion-collection/4"' type="button" class="btn btn-light btn-sm">{{__('homepage.inspect_collection')}}</button>
                                        @endif
                                    </li>
                                </ul>
                            </article>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card boy">
                        <img class="card-img" src="/img/koleksiyon-üst-kısım-erkek-çocuk-modified.jpg" style="height: 270px">
                        <div class="card-img-overlay">
                            <article>
                                <ul class="list-unstyled">
                                    <li><h5>{{__('homepage.boy_collection')}}</h5></li>
                                    <li class="mt-3">
                                        @if(session('locale') === 'tr')
                                            <button onclick='window.location.href="/{{session('locale')}}/collections/erkek-cocuk-koleksiyonu/5"' type="button" class="btn btn-light btn-sm">{{__('homepage.inspect_collection')}}</button>
                                        @else
                                            <button onclick='window.location.href="/{{session('locale')}}/collections/boy-fashion-collection/5"' type="button" class="btn btn-light btn-sm">{{__('homepage.inspect_collection')}}</button>
                                        @endif
                                    </li>
                                </ul>
                            </article>

                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card opportunity">
                        <img class="card-img" src="/collections/collection-5c12f9a403510.png" style="max-height: 350px">
                        <div class="card-img-overlay">
                            <article>
                                <ul class="list-unstyled">
                                    <li><h5>{{__('homepage.outlet_collection')}}</h5></li>
                                    <li class="mt-3">
                                        <button onclick='window.location.href="/{{session('locale')}}/collections/outlet/7"' type="button" class="btn btn-light btn-sm">{{__('homepage.inspect_collection')}}</button>
                                    </li>
                                </ul>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Middle About-->
    <div class="himms-about" data-aos="fade-up">
        <div class="container">
            <div class="col-md-2 mx-auto">
                <img class="img-fluid " src="/img/himms.png">
            </div>
            <div class="col-md-10 offset-lg-1 mt-4 text-center">
                <p>
                    {{__('about.title')}}
                </p>
            </div>
            <div class="col-md-2 mx-auto mt-4 text-center">
                <button type="button" class="btn btn-light" onclick="window.location.href='/{{session('locale')}}/about'">
                    {{__('homepage.read_more')}}
                </button>
            </div>
            <div class="row text-center mt-5">
                <div class="col-lg-4 col-sm-4">
                    <ul class="list-unstyled">
                        <li>
                            <h1 id="js-count-1">{{__('counter.product_count')}}</h1>
                        </li>
                        <li>
                            <span>{{__('counter.product')}}</span>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-sm-4">
                    <ul class="list-unstyled">
                        <li>
                            <h1 id="js-count-2">{{__('counter.customer_count')}}</h1>
                        </li>
                        <li>
                            <span>{{__('counter.customer')}}</span>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-sm-4">
                    <ul class="list-unstyled">
                        <li>
                            <h1 id="js-count-3">{{__('counter.experience_count')}}</h1>
                        </li>
                        <li>
                            <span>{{__('counter.experience')}}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection