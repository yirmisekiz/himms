@extends('layouts.public')

@section('content')

    <!-- Sub Header-->
    <div class="sub-header news">
        <div class="container">
            <div class="col- offset-lg-1">
                <ul class="list-unstyled">
                    <li><h4>Himm's</h4></li>
                    <li><h5 class="pink">{{__('menu.news')}}</h5></li>
                    <hr>
                </ul>
            </div>
        </div>
    </div>

    <div class="content mt-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 p-0 sub-menu">
                    <ul class="list-unstyled">
                        <li>
                            <a href="/{{session('locale')}}/about"> {{__('menu.about')}} </a>
                        </li>
                        <li>
                            <a href="/{{session('locale')}}/shops"> {{__('menu.shops')}} </a>
                        </li>
                        <li class="active">
                            <a href="/{{session('locale')}}/news"> {{__('menu.news')}} </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-8 offset-lg-1 mt-2">
                    <div class="news">
                        @foreach($news as $new)
                            <div class="row" data-aos="fade-up">
                                <div class="col-md-12">
                                    <ul class="list-unstyled mb-3">
                                        <li><h5 class="m-0">{{$new->$title}}</h5></li>
                                        <li>
                                            <small>{{$new->created_at_formatted}}</small>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <img class="img-thumbnail" src="{{env('APP_URL') . $new->image}}">
                                </div>
                                <div class="col-md-6">
                                    <p class="mt-3">
                                        {{$new->$description}}
                                    </p>
                                </div>
                            </div>
                            <hr class="col- mt-3 mb-3">
                        @endforeach
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection