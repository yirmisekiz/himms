@extends('layouts.public')

@section('content')
    <!-- Sub Header-->
    <div class="sub-header about">
        <div class="container">
            <div class="col- offset-lg-1">
                <ul class="list-unstyled">
                    <li><h4>Himm's</h4></li>
                    <li><h5>{{__('menu.about')}}</h5></li>
                    <hr>
                </ul>
            </div>
        </div>
    </div>

    <div class="content mt-4">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 p-0 sub-menu">
                    <ul class="list-unstyled">
                        <li class="active">
                            <a href="/{{session('locale')}}/about">
                                {{__('menu.about')}}
                            </a>
                        </li>

                        <li>
                            <a href="/{{session('locale')}}/shops">
                                {{__('menu.shops')}}
                            </a>
                        </li>
                        <li>
                            <a href="/{{session('locale')}}/news">
                                {{__('menu.news')}}
                            </a>
                        </li>

                    </ul>
                </div>

                <div class="col-md-8 offset-lg-1 mt-2">

                    <div class="about-text">

                        <p class="font-weight-bold">
                            {{__('about.title')}}
                        </p>

                        <p>
                            {{__('about.description_1')}}
                        </p>

                        <p>
                            {{__('about.description_2')}}
                        </p>

                        <p>
                            {{__('about.description_3')}}
                        </p>

                        <p>
                            {{__('about.description_4')}}
                        </p>

                        <p>
                            {{__('about.description_5')}}
                        </p>

                    </div>

                </div>

            </div>

            <div class="himms-about">

                <div class="row text-center">
                    <div class="col-lg-3 col-sm-4 offset-lg-3">
                        <ul class="list-unstyled">
                            <li>
                                <h1 id="js-count-1">{{__('counter.product_count')}}</h1>
                            </li>
                            <li>
                                <span>{{__('counter.product')}}</span>
                            </li>
                        </ul>

                    </div>

                    <div class="col-lg-3 col-sm-4">
                        <ul class="list-unstyled">
                            <li>
                                <h1 id="js-count-2">{{__('counter.customer_count')}}</h1>
                            </li>
                            <li>
                                <span>{{__('counter.customer')}}</span>
                            </li>
                        </ul>

                    </div>

                    <div class="col-lg-3 col-sm-4">
                        <ul class="list-unstyled">
                            <li>
                                <h1 id="js-count-3">{{__('counter.experience_count')}}</h1>
                            </li>
                            <li>
                                <span>{{__('counter.experience')}}</span>
                            </li>
                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </div>



@endsection