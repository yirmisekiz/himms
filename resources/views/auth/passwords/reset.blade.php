@extends('layouts.admin')

@section('content')

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h2 >Şifre Sıfırlama</h2>
            </div>
            <form class="m-t" role="form" method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    <input placeholder="E-posta" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                </div>
                <div class="form-group">
                    <input placeholder="Yeni Şifreniz" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                </div>
                <div class="form-group">
                    <input placeholder="Yeni Şifreniz Tekrar" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Onayla</button>
            </form>
        </div>
    </div>
@endsection
