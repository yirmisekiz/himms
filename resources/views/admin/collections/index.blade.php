@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Koleksiyonlar</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Ürün Adedi</th>
                                    <th>Yönetim</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($collections as $collection)
                                        <tr>
                                            <td>
                                                {{$collection->name_tr}}
                                            </td>
                                            <td>
                                                {{$collection->products->count()}}
                                            </td>
                                            <td>
                                                <a target="_blank" style="margin-right: 10px" class="text-navy" href="/tr/collections/{{$collection->slug_tr}}/{{$collection->id}}">Görüntüle</a>
                                                @if($collection->is_passive === 1)
                                                    <a style="margin-right: 10px" class="text-navy" href="/tr/admin/collections/{{$collection->id}}/activate" onclick="return sweetConfirm('Koleksiyon artık görünür olacaktır, emin misiniz?')">Aktifleştir</a>
                                                @else
                                                    <a style="margin-right: 10px" class="text-warning" href="/tr/admin/collections/{{$collection->id}}/passive" onclick="return sweetConfirm('Koleksiyon müşterileriniz tarafından görülmeyecek, emin misiniz?')">Pasifleştir</a>
                                                @endif
                                                <a style="margin-right: 10px" href="/tr/admin/collections/{{$collection->id}}/edit">Düzenle</a>
                                                <a style="margin-right: 10px" class="text-danger" href="/tr/admin/collections/{{$collection->id}}/delete" onclick="return sweetConfirm('Koleksiyonu silmek istediğinizden emin misiniz?')">Sil</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                "language": {
                    "sDecimal": ",",
                    "sEmptyTable": "Tabloda herhangi bir veri mevcut değil",
                    "sInfo": "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
                    "sInfoEmpty": "Kayıt yok",
                    "sInfoFiltered": "(_MAX_ kayıt içerisinden bulunan)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "Sayfada _MENU_ kayıt göster",
                    "sLoadingRecords": "Yükleniyor...",
                    "sProcessing": "İşleniyor...",
                    "sSearch": "Ara:",
                    "sZeroRecords": "Eşleşen kayıt bulunamadı",
                    "oPaginate": {
                        "sFirst": "İlk",
                        "sLast": "Son",
                        "sNext": "Sonraki",
                        "sPrevious": "Önceki"
                    },
                    "oAria": {
                        "sSortAscending": ": artan sütun sıralamasını aktifleştir",
                        "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
                    },
                    "select": {
                        "rows": {
                            "_": "%d kayıt seçildi",
                            "0": "",
                            "1": "1 kayıt seçildi"
                        }
                    }
                },
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
        });
    </script>
@endsection