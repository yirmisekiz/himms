@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Yeni Koleksiyon</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        @include('admin.partials.validation_errors')

                        <form method="POST" class="form-horizontal" action="/tr/admin/collections/store">
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fotoğraf Yükleme</label>
                                <div class="col-sm-10" id="js-dropzone-inputs">
                                    <!-- You can add extra form fields here -->
                                    <div class="dropzone dropzone-file-area" id="fileUpload">
                                        <div class="dz-default dz-message">
                                            <h3 class="sbold">Dosyayı buraya sürükleyerek yükleyebilirsiniz</h3>
                                            <span>Ayrıca buraya tıklayarak dosyada seçebilirsiniz</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Koleksiyon Adı</label>
                                <div class="col-sm-10"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Türkçe</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ad</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="{{old('name_tr')}}" name="name_tr">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">İngilizce</label>
                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ad</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name_en" value="{{old('name_en')}}">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Arapça</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ad</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name_sa" value="{{old('name_sa')}}">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Rusça</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ad</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name_ru" value="{{old('name_ru')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Görünüm adresi</label>
                                <div class="col-sm-10"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Türkçe</label>
                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Adres</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="{{old('slug_tr')}}" name="slug_tr">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">İngilizce</label>
                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Adres</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="{{old('slug_en')}}" name="slug_en">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Kaydet</button>
                                        <a class="btn btn-white" href="/tr/admin/collections">Vazgeç</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            Dropzone.options.fileUpload = {
                url: 'blackHole.php',
                addRemoveLinks: true,
                maxFiles: 1,
                init: function (file) {
                    this.on("addedfile", function (event) {
                        while (this.files.length > this.options.maxFiles) {
                            swal({
                                title: '',
                                text: 'Koleksiyonlara en fazla 1 fotoğraf eklenebilir.',
                                type: "info",
                                allowOutsideClick: true,
                            });
                            this.removeFile(this.files[0]);
                        }
                    });
                },
                accept: function (file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    fileReader.onloadend = function () {

                        var content = fileReader.result;
                        $('#js-dropzone-inputs').append('<input hidden name="file[]" value="' + content + '">');
                        file.previewElement.classList.add("dz-success");
                    };
                    file.previewElement.classList.add("dz-complete");
                }
            };

            $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: "dd/mm/yyyy"
            });
        });
    </script>
@endsection