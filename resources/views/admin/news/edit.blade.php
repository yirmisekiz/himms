@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Haber Düzenleme</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form method="POST" class="form-horizontal" action="/tr/admin/news/{{$news->id}}/update">
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fotoğraf Yükleme</label>
                                <input hidden id="js-uploaded-images" type="text" value="{{json_encode([0=>['path'=>$news->image]])}}">
                                <div class="col-sm-10" id="js-dropzone-inputs">
                                    <!-- You can add extra form fields here -->
                                    <div class="dropzone dropzone-file-area" id="fileUpload">
                                        <div class="dz-default dz-message">
                                            <h3 class="sbold">Dosyayı buraya sürükleyerek yükleyebilirsiniz</h3>
                                            <span>Ayrıca buraya tıklayarak dosyada seçebilirsiniz</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Haber Tarihi</label>

                                <div class="col-sm-3" id="data_1">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="created_at" type="text" class="form-control" value="{{$news->created_at->format('d/m/Y')}}">
                                    </div>
                                </div>
                                <div class="col-sm-offset-7"></div>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Türkçe</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="{{$news->title_tr}}" name="title_tr">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea style="resize:none" rows="5" type="text" class="form-control" name="description_tr">{{$news->description_tr}}</textarea>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Arapça</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title_sa" value="{{$news->title_sa}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea style="resize:none" rows="5" type="text" class="form-control" name="description_sa">{{$news->description_sa}}</textarea>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Rusça</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title_ru" value="{{$news->title_ru}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea style="resize:none" rows="5" type="text" class="form-control" name="description_ru">{{$news->description_ru}}</textarea>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">İngilizce</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title_en" value="{{$news->title_en}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea style="resize:none" rows="5" type="text" class="form-control" name="description_en">{{$news->description_en}}</textarea>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a class="btn btn-white" href="/tr/admin/news">Vazgeç</a>
                                        <button class="btn btn-primary" type="submit">Kaydet</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {


            Dropzone.options.fileUpload = {
                url: 'blackHole.php',
                addRemoveLinks: true,
                maxFiles: 1,
                init: function (file) {

                    var $this = this;
                    var xmlStack = [];
                    var otherStack = [];

                    $.each(jQuery.parseJSON($('#js-uploaded-images').val()), function (i, e) {

                        if (e.path === null) {
                            return false;
                        }

                        (function (i) {
                            var mockFile = {name: 'image-' + i, size: 12345, type: 'image/jpeg'};
                            $this.options.addedfile.call($this, mockFile);
                            var url = '{{env('APP_URL')}}' + e.path;
                            $this.options.thumbnail.call($this, mockFile, url);
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');

                            xmlStack[i] = new XMLHttpRequest();
                            xmlStack[i].open('GET', url, true);
                            xmlStack[i].responseType = 'arraybuffer';
                            xmlStack[i].onload = function (e) {
                                otherStack[i] = new Uint8Array(this.response);
                                var b64 = btoa(new Uint8Array(otherStack[i]).reduce(function (data, byte) {
                                    return data + String.fromCharCode(byte);
                                }, ''));
                                var dataURL = "data:image/jpeg;base64," + b64;
                                $('#js-dropzone-inputs').append('<input class="image-' + i + '" hidden name="file[]" value="' + dataURL + '">');
                            };
                            xmlStack[i].send();
                        })(i);

                    });

                    this.on("addedfile", function (event) {
                        while (this.files.length > this.options.maxFiles) {
                            swal({
                                title: '',
                                text: 'Haberlere en fazla 1 fotoğraf eklenebilir.',
                                type: "info",
                                allowOutsideClick: true,
                            });
                            this.removeFile(this.files[0]);
                        }
                    });
                },
                removedfile: function (file, data) {
                    $('img[alt=' + file.name + ']').parent().parent().remove();
                    $('.' + file.name).remove();
                },
                accept: function (file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    fileReader.onloadend = function () {

                        var content = fileReader.result;
                        $('#js-dropzone-inputs').append('<input hidden name="file[]" value="' + content + '">');
                        file.previewElement.classList.add("dz-success");
                    };
                    file.previewElement.classList.add("dz-complete");
                }
            };

            $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: "dd/mm/yyyy"
            });
        });
    </script>
@endsection