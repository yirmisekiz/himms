@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Yeni Haber</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form method="POST" class="form-horizontal" action="/tr/admin/news/store">
                            @csrf

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Haber Tarihi</label>

                                <div class="col-sm-3" id="news-datepicker">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="created_at" type="text" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="col-sm-offset-7"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fotoğraf Yükleme</label>
                                <div class="col-sm-10" id="js-dropzone-inputs">
                                    <!-- You can add extra form fields here -->
                                    <div class="dropzone dropzone-file-area" id="fileUpload">
                                        <div class="dz-default dz-message">
                                            <h3 class="sbold">Dosyayı buraya sürükleyerek yükleyebilirsiniz</h3>
                                            <span>Ayrıca buraya tıklayarak dosyada seçebilirsiniz</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Türkçe</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="" name="title_tr">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea style="resize:none" rows="5" type="text" class="form-control" name="description_tr"></textarea>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Arapça</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title_sa" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea style="resize:none" rows="5" type="text" class="form-control" name="description_sa"></textarea>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Rusça</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title_ru" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea style="resize:none" rows="5" type="text" class="form-control" name="description_ru"></textarea>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">İngilizce</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title_en" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea style="resize:none" rows="5" type="text" class="form-control" name="description_en"></textarea>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Kaydet</button>
                                        <a class="btn btn-white" href="/tr/admin/news">Vazgeç</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            Dropzone.options.fileUpload = {
                url: 'blackHole.php',
                addRemoveLinks: true,
                maxFiles: 1,
                init: function (file) {
                    this.on("addedfile", function (event) {
                        while (this.files.length > this.options.maxFiles) {
                            swal({
                                title: '',
                                text: 'Haberlere en fazla 1 fotoğraf eklenebilir.',
                                type: "info",
                                allowOutsideClick: true,
                            });
                            this.removeFile(this.files[0]);
                        }
                    });
                },
                accept: function (file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    fileReader.onloadend = function () {

                        var content = fileReader.result;
                        $('#js-dropzone-inputs').append('<input hidden name="file[]" value="' + content + '">');
                        file.previewElement.classList.add("dz-success");
                    };
                    file.previewElement.classList.add("dz-complete");
                }
            };
        });
    </script>
@endsection