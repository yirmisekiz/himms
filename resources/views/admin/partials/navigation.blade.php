<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">

        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header text-center">
                <figure class="text-center">
                    <img class="img-fluid" src="/img/logo@3x.png">
                </figure>
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{auth()->user()->name}}</strong>
                            </span>
                        </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li>
                            <a class="dropdown-item" href="/"> Siteyi Görüntüle </a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"> Çıkış Yap </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>

                <div class="logo-element">
                    H
                </div>
            </li>
            <li>
                <a href="/tr/admin"><i class="fa fa-users"></i> <span class="nav-label">Yönetim Paneli</span></a>
            </li>
            <li>
                <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Kullanıcı</span>
                    <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/tr/admin/users">Kullanıcılarım</a></li>
                    <li><a href="/tr/admin/users/create">Yeni Kullanıcı</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-star"></i> <span class="nav-label">Koleksiyon</span>
                    <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/tr/admin/collections">Koleksiyonlarım</a></li>
                    <li><a href="/tr/admin/collections/create">Yeni Koleksiyon</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-suitcase"></i>
                    <span class="nav-label">Ürün</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/tr/admin/products">Ürünlerim</a></li>
                    <li><a href="/tr/admin/products/create">Yeni Ürün</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-newspaper-o"></i>
                    <span class="nav-label">Haber</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="/tr/admin/news">Haberlerim</a></li>
                    <li><a href="/tr/admin/news/create">Yeni Haber</a></li>
                </ul>
            </li>
            <li>
                <a href="{{ url('/tr/admin/subscriptions') }}"><i class="fa fa-envelope"></i>
                    <span class="nav-label">E-posta</span> </a>
            </li>
            <li>
                <?php
                if(file_exists(app_path() . '/../storage/framework/down')){ ?>
                    <a href="{{ url('/tr/admin/up') }}"><i class="fa fa-space-shuttle"></i>
                        <span class="nav-label">Canlı Mod</span> </a>
                <?php }else{ ?>
                    <a href="{{ url('/tr/admin/down') }}"><i class="fa fa-space-shuttle"></i>
                        <span class="nav-label">Bakım Modu</span> </a>
                <?php } ?>
            </li>
        </ul>

    </div>
</nav>
