@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{{$product->name_tr}} Düzenleme</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        @include('admin.partials.validation_errors')

                        <form method="POST" class="form-horizontal" action="/tr/admin/products/{{$product->id}}/update">
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fotoğraf Yükleme</label>
                                <input hidden id="js-uploaded-images" type="text" value="{{json_encode($product->images->toArray())}}">
                                <div class="col-sm-10" id="js-dropzone-inputs">
                                    <!-- You can add extra form fields here -->
                                    <div class="dropzone dropzone-file-area" id="fileUpload">
                                        <div class="dz-default dz-message">
                                            <h3 class="sbold">Dosyayı buraya sürükleyerek yükleyebilirsiniz</h3>
                                            <span>Ayrıca buraya tıklayarak dosyada seçebilirsiniz</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Koleksiyonlar</label>
                                <div class="col-sm-10">
                                    <select class="js-select-collection form-control" multiple name="collections[]">
                                        <option></option>
                                        @foreach($collections as $collection)
                                            <option value="{{$collection->id}}">{{$collection->name_tr}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kategori</label>
                                <div class="col-sm-10">
                                    <select class="js-select-age form-control" name="category_id">
                                        <option></option>
                                        @foreach($categories as $category)
                                            @if(strval($product->category_id) === strval($category->id))
                                                <option selected value="{{$category->id}}">{{$category->name_tr}}</option>
                                            @else
                                                <option value="{{$category->id}}">{{$category->name_tr}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ürün Özellikleri</label>
                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ad</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="{{$product->name}}" name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kod</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="{{$product->code}}" name="code">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Yaş Seçenekleri</label>
                                    <div class="col-sm-10">
                                        <select class="js-select-age form-control" name="age_id">
                                            <option></option>
                                            @foreach($ages as $age)
                                                @if($product->age_id === $age->id)
                                                    <option selected value="{{$age->id}}">{{$age->name_tr}}</option>
                                                @else
                                                    <option value="{{$age->id}}">{{$age->name_tr}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Renk Seçenekleri</label>
                                    <div class="col-sm-10">
                                        <input hidden id="js-color-options" value="{{json_encode($colors)}}">
                                        <select class="js-select-colors form-control" multiple name="colors[]"></select>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Türkçe</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="{{$product->description_tr}}" name="description_tr">
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Arapça</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name_sa" value="{{$product->name_sa}}">
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Rusça</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name_ru" value="{{$product->name_ru}}">
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">İngilizce</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Başlık</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name_en" value="{{$product->name_en}}">
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Kaydet</button>
                                        <a class="btn btn-white" href="/tr/admin/products">Vazgeç</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            Dropzone.options.fileUpload = {
                url: 'blackHole.php',
                addRemoveLinks: true,
                clickable: true,
                init: function (file) {

                    var $this = this;
                    var xmlStack = [];
                    var otherStack = [];

                    $.each(jQuery.parseJSON($('#js-uploaded-images').val()), function (i, e) {

                        if (e.path === null) {
                            return false;
                        }

                        (function (i) {
                            var mockFile = {name: 'image-' + i, size: 12345, type: 'image/jpeg'};
                            $this.options.addedfile.call($this, mockFile);
                            var url = '{{env('APP_URL')}}' + e.path;
                            $this.options.thumbnail.call($this, mockFile, url);
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');

                            xmlStack[i] = new XMLHttpRequest();
                            xmlStack[i].open('GET', url, true);
                            xmlStack[i].responseType = 'arraybuffer';
                            xmlStack[i].onload = function (e) {
                                otherStack[i] = new Uint8Array(this.response);
                                var b64 = btoa(new Uint8Array(otherStack[i]).reduce(function (data, byte) {
                                    return data + String.fromCharCode(byte);
                                }, ''));
                                var dataURL = "data:image/jpeg;base64," + b64;
                                $('#js-dropzone-inputs').append('<input class="image-' + i + '" hidden name="file[]" value="' + dataURL + '">');
                            };
                            xmlStack[i].send();
                        })(i);

                    });

                },
                removedfile: function (file, data) {
                    $('img[alt=' + file.name + ']').parent().parent().remove();
                    $('.' + file.name).remove();
                },
                accept: function (file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    fileReader.onloadend = function () {

                        var content = fileReader.result;
                        $('#js-dropzone-inputs').append('<input hidden name="file[]" value="' + content + '">');
                        file.previewElement.classList.add("dz-success");
                    };
                    file.previewElement.classList.add("dz-complete");
                }
            };

            $('.js-select-collection').val({{json_encode($product->collections->pluck('id'))}}).trigger('change');

            $('.js-select-colors').val({{json_encode($product->colors->pluck('id'))}}).trigger('change');

        });
    </script>
@endsection