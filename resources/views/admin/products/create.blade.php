@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Yeni Ürün</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        @include('admin.partials.validation_errors')

                        <form method="POST" class="form-horizontal" action="/tr/admin/products/store">
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Fotoğraf Yükleme</label>

                                <div class="col-sm-10" id="js-dropzone-inputs">

                                    <!-- You can add extra form fields here -->

                                    <div class="dropzone dropzone-file-area" id="fileUpload">
                                        <div class="dz-default dz-message">
                                            <h3 class="sbold">Dosyayı buraya sürükleyerek yükleyebilirsiniz</h3>
                                            <span>Ayrıca buraya tıklayarak dosyada seçebilirsiniz</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Koleksiyonlar</label>
                                <div class="col-sm-10">
                                    <select class="js-select-collection form-control" multiple name="collections[]">
                                        <option></option>
                                        @foreach($collections as $collection)
                                            <option value="{{$collection->id}}">{{$collection->name_tr}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kategoriler</label>
                                <div class="col-sm-10">
                                    <select class="js-select-collection form-control" name="category_id">
                                        <option></option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name_tr}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>

                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ürün Özellikleri</label>
                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ad</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="" name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kod</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" value="" name="code">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Yaş Seçenekleri</label>
                                    <div class="col-sm-10">
                                        <select class="js-select-age form-control" name="age_id">
                                            <option></option>
                                            @foreach($ages as $age)
                                                <option value="{{$age->id}}">{{$age->name_tr}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Renk Seçenekleri</label>
                                    <div class="col-sm-10">
                                        <input hidden id="js-color-options" value="{{json_encode($colors)}}">
                                        <select class="js-select-colors form-control" multiple name="colors[]"></select>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>

                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ürün Açıklamaları</label>
                                    <div class="col-sm-10"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Türkçe</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea type="text" rows="4" class="form-control" value="{{old('description_tr')}}" name="description_tr"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Arapça</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea type="text" rows="4" class="form-control" name="description_sa" value=""></textarea>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Rusça</label>

                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea type="text" rows="4" class="form-control" name="description_ru" value=""></textarea>
                                    </div>
                                </div>

                            </div>
                            <div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">İngilizce</label>
                                    <div class="col-sm-10"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Açıklama</label>
                                    <div class="col-sm-10">
                                        <textarea type="text" rows="4" class="form-control" name="description_en" value=""></textarea>
                                    </div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" id="submit-all" type="submit">Kaydet</button>
                                        <a class="btn btn-white" href="/tr/admin/news">Vazgeç</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            Dropzone.options.fileUpload = {
                url: 'blackHole.php',
                addRemoveLinks: true,
                accept: function (file) {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    fileReader.onloadend = function () {

                        var content = fileReader.result;
                        $('#js-dropzone-inputs').append('<input hidden name="file[]" value="' + content + '">');
                        file.previewElement.classList.add("dz-success");
                    };
                    file.previewElement.classList.add("dz-complete");
                }
            };

        });
    </script>
@endsection