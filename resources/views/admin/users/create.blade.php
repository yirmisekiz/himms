@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Yeni Kullanıcı Formu</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form method="POST" class="form-horizontal" action="/tr/admin/users/store">
                            @csrf
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Ad - Soyad</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="Ad Soyad" name="name" required autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">E-posta Adresi</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="E-posta adresi" name="email" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Şifre</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" name="password" placeholder="Şifre" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Şifre Tekrar</label>
                                <div class="col-sm-10">
                                    <input type="text" placeholder="Şifre Tekrar" class="form-control" name="password_confirmation">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Kaydet</button>
                                    <a href="/tr/admin/users" class="btn btn-default">Vazgeç</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
        });
    </script>
@endsection