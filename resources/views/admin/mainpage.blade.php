@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Koleksiyonlarım</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{$collectionCount}}</h1>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ürünlerim</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{$productCount}}</h1>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>E-postalar</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{$subscriptionCount}}</h1>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Haberlerim</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">{{$newsCount}}</h1>
                </div>
            </div>
        </div>
    </div>
@endsection
