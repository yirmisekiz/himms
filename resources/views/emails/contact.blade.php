@component('mail::message')
# İletişim Formu Dolduruldu

Sebep: {{$data['reason']}}
<br>
İsim: {{$data['name']}}
<br>
E-posta: {{$data['email']}}
<br>
Mesaj: {{$data['message']}}

İletişime geçmek için lütfen {{$data['email']}} adresine dönüş yapınız.

Teşekkürler,<br>
{{ config('app.name') }}
@endcomponent
