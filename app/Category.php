<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    protected $fillable
        = [
            'name_tr',
            'name_sa',
            'name_en',
            'name_ru',
        ];

}
