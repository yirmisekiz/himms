<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ColorProduct extends Pivot {

    use SoftDeletes;

    protected $dates
        = [
            'created_at',
            'modified_at',
            'deleted_at'
        ];

    protected $fillable
        = [
            'product_id',
            'color_id',
            'created_at',
            'modified_at',
            'deleted_at'
        ];

}
