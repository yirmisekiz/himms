<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Age extends Model {

    protected $fillable
        = [
            'name_tr',
            'name_sa',
            'name_en',
            'name_ru',
        ];

}
