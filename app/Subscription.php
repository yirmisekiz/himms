<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model {

    protected $fillable
        = [
            'created_at',
            'updated_at',
            'email',
        ];

}
