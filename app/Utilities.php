<?php

namespace App;

trait Utilities {

    static function checkSlugValid($slug) {

        if (preg_match('/^[a-z0-9]+(?:-[a-z0-9]+)*$/', $slug) === 1) {
            return true;
        }

        return false;
    }

}
