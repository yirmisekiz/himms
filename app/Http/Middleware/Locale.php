<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use function is_null;
use function session;
use function strpos;

class Locale {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next) {

        if (is_null(session('locale'))) {

            $lang = $request->getPreferredLanguage();
            if (strpos($lang, 'en') !== false) {
                session(['locale' => 'en']);
                app()->setLocale('en');
            } elseif (strpos($lang, 'sa') !== false) {
                session(['locale' => 'sa']);
                app()->setLocale('sa');
            } elseif (strpos($lang, 'ru') !== false) {
                session(['locale' => 'ru']);
                app()->setLocale('ru');
            } else {
                session(['locale' => 'tr']);
                app()->setLocale('tr');
            }
        } else {
            if ($request->method() === 'GET' || ! (count(array_intersect(['css', 'js', 'admin', 'login', 'registration'], $request->segments())) > 0)) {

                $segment = $request->segment(1);

                if ( ! in_array($segment, config('app.locales'))) {
                    $segments = $request->segments();
                    $fallback = session('locale') ?: config('app.fallback_locale');
                    $segments = array_prepend($segments, $fallback);

                    return redirect()->to(implode('/', $segments));
                }

                session(['locale' => $segment]);
                app()->setLocale($segment);
            }
        }

        return $next($request);
    }
}