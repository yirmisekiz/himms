<?php

namespace App\Http\Controllers\Admin;

use App\News;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class NewsController extends Controller {

    public function __construct() {

        $this->middleware('auth');
    }

    public function index() {

        $news = News::get();
        $title = 'title_' . (session('locale'));
        $description = 'description_' . (session('locale'));

        return view('admin.news.index', compact('news', 'title', 'description'));
    }

    public function store(Request $request) {

        if ($request->get('created_at')) {
            $carbon = Carbon::createFromFormat('d/m/Y', $request->get('created_at'));
            $data = $request->toArray();
            $data['created_at'] = $carbon;
        }

        $news = new News($data);

        $news->save();
        if (isset($request->get('file')[0])) {
            $base64 = $request->get('file')[0];
            $extension = $this->getExtension($request->get('file')[0]);
            $path = "/news/news-" . uniqid() . $extension;
            file_put_contents(app_path() . '/../public' . $path, file_get_contents($base64));
            $news->image = $path;
            $news->save();
        }

        return redirect('/tr/admin/news')->with('success', Config::get('constants.messages.operation_success'));

    }

    public function edit(Request $request, $locale, News $news) {

        return view('admin.news.edit', compact('news'));
    }

    public function create(Request $request, $locale) {

        return view('admin.news.create');

    }

    public function delete(Request $request, $locale, News $news) {

        $news->delete();

        return redirect()->back();
    }

    public function update(Request $request, $locale, News $news) {

        if ($request->get('created_at')) {
            $carbon = Carbon::createFromFormat('d/m/Y', $request->get('created_at'));
            $data = $request->toArray();
            $data['created_at'] = $carbon;
        }

        $news->update($data);

        if (isset($request->get('file')[0])) {
            $base64 = $request->get('file')[0];
            $extension = $this->getExtension($request->get('file')[0]);
            $path = "/news/news-" . uniqid() . $extension;
            file_put_contents(app_path() . '/../public' . $path, file_get_contents($base64));
            $news->image = $path;
            $news->save();
        } else {
            $news->image = null;
            $news->save();
        }

        return redirect('/tr/admin/news');

    }

}
