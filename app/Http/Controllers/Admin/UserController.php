<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware('auth');
    }

    public function index() {

        $users = User::withTrashed()->get();

        return view('admin.users.index', compact('users'));
    }

    public function create() {

        return view('admin.users.create');
    }

    public function delete(Request $request, $locale, User $user) {

        if ($user->delete()) {
            return redirect()->back()->with('success', 'İşlem başarılı.');
        };

        return redirect()->back()->with('error', 'İşlem başarısız.');
    }

    public function activate(Request $request, $lang, $userId) {

        $user = User::withTrashed()->where('id', $userId)->first();
        if ($user->restore()) {
            return redirect()->back()->with('success', 'İşlem başarılı.');
        };

        return redirect()->back()->with('error', 'İşlem başarısız.');

    }

    public function store(Request $request) {

        $valid = Validator::make($request->all(), [
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($valid->fails()) {
            return redirect()->back()->with('error', Config::get('constants.messages.validation_failure'));
        }

        User::create([
            'name'     => $request->get('name'),
            'email'    => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        return redirect('/tr/admin/users')->with('success', Config::get('constants.messages.operation_success'));
    }

}
