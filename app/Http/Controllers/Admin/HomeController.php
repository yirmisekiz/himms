<?php

namespace App\Http\Controllers\Admin;

use App\Collection;
use App\Http\Controllers\Controller;
use App\News;
use App\Product;
use App\Subscription;
use function compact;
use Illuminate\Support\Facades\Artisan;
use function redirect;

class HomeController extends Controller {

    public function __construct() {

        $this->middleware('auth');
    }

    public function down() {

        Artisan::call('down');

        return redirect()->back()->with('success','Siteniz bakım moduna alınmıştır.');
    }

    public function up() {

        Artisan::call('up');

        return redirect()->back()->with('success','Siteniz tekrar canlı moda alınmıştır.');
    }

    public function index() {

        $productCount = Product::count();
        $collectionCount = Collection::count();
        $subscriptionCount = Subscription::count();
        $newsCount = News::count();

        return view('admin.mainpage', compact('productCount', 'collectionCount', 'subscriptionCount', 'newsCount'));
    }

}
