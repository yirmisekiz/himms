<?php

namespace App\Http\Controllers\Admin;

use App\Age;
use App\Category;
use App\Color;
use App\Http\Requests\StoreProduct;
use App\Product;
use App\Collection;
use App\ProductColors;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use function redirect;

class ProductController extends Controller {

    public function __construct() {

        $this->middleware('auth');
    }

    public function index() {

        $products = Product::get();

        return view('admin.products.index', compact('products'));
    }

    public function store(StoreProduct $request) {

        $product = new Product($request->toArray());

        if ( ! $product->save()) {
            return redirect()->back()->with('error', Config::get('constants.messages.operation_error'))->withInput();
        };

        if ( ! is_null($request->get('colors'))) {
            $product->colors()->sync($request->toArray()['colors']);
        }

        if ( ! is_null($request->get('collections'))) {
            $product->collections()->sync($request->toArray()['collections']);
        }

        if ( ! is_null($request->get('file'))) {
            $images = [];
            foreach ($request->get('file') as $base64) {

                $extension = $this->getExtension($base64);
                $path = "/products/product-" . uniqid() . $extension;
                file_put_contents(app_path() . '/../public' . $path, file_get_contents($base64));

                $pI = new \App\ProductImage();
                $pI->path = $path;
                array_push($images, $pI);
            }

            $product->images()->saveMany($images);
        }

        return redirect('/tr/admin/products')->with('success', Config::get('constants.messages.operation_success'));

    }

    public function create() {

        $categories = Category::all();
        $collections = Collection::all();
        $ages = Age::all();
        $colors = Color::all();

        return view('admin.products.create', compact('ages', 'colors', 'collections', 'categories'));

    }

    public function update(StoreProduct $request, $locale, Product $product) {

        if ( ! $product->update($request->toArray())) {
            return redirect()->back()->with('error', Config::get('constants.messages.operation_error'))->withInput();
        };

        if ( ! is_null($request->get('collections'))) {
            $product->collections()->sync($request->toArray()['collections']);
        }

        if ( ! is_null($request->get('colors'))) {
            $product->colors()->sync($request->toArray()['colors']);
        }

        if ( ! is_null($request->get('file'))) {

            $product->images()->delete();

            $images = [];
            foreach ($request->get('file') as $base64) {

                $extension = $this->getExtension($base64);
                $path = "/products/product-" . uniqid() . $extension;
                file_put_contents(app_path() . '/../public' . $path, file_get_contents($base64));

                $pI = new \App\ProductImage();
                $pI->path = $path;
                array_push($images, $pI);
            }

            $product->images()->saveMany($images);

        }

        return redirect('/tr/admin/products')->with('success', Config::get('constants.messages.operation_success'));

    }

    public function delete(Request $request, $locale, Product $product) {

        $product->delete();

        return redirect('/tr/admin/products')->with('success', Config::get('constants.messages.operation_success'));

    }

    public function edit(Request $request, $local, Product $product) {

        $collections = Collection::all();
        $ages = Age::all();
        $colors = Color::all();
        $categories = Category::all();

        return view('admin.products.edit', compact('product', 'collections', 'colors', 'ages', 'categories'));

    }

    public function passive(Request $request, $local, Product $product) {

        $product->is_passive = 1;
        $product->save();

        return redirect()->back()->with('success', Config::get('constants.messages.operation_success'));
    }

    public function activate(Request $request, $local, Product $product) {

        $product->is_passive = null;
        $product->save();

        return redirect()->back()->with('success', Config::get('constants.messages.operation_success'));
    }

}
