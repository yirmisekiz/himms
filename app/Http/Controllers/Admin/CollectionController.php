<?php

namespace App\Http\Controllers\Admin;

use App\Collection;
use App\Http\Requests\StoreCollection;
use function app_path;
use Exception;
use function file_put_contents;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use function is_null;
use function redirect;
use function str_slug;

class CollectionController extends Controller {

    public function __construct() {

        $this->middleware('auth');
    }

    public function passive(Request $request, $local, Collection $collection) {

        $collection->is_passive = 1;
        $collection->save();

        return redirect()->back()->with('success', Config::get('constants.messages.operation_success'));
    }

    public function activate(Request $request, $local, Collection $collection) {
        $collection->is_passive = null;
        $collection->save();

        return redirect()->back()->with('success', Config::get('constants.messages.operation_success'));;

    }

    public function index() {

        $collections = Collection::all();

        return view('admin.collections.index', compact('collections'));
    }

    public function create(Request $request) {

        return view('admin.collections.create');
    }

    public function delete(Request $request, $local, Collection $collection) {

        $collection->delete();

        return redirect('/tr/admin/collections')->with('success', Config::get('constants.messages.operation_success'));

    }

    public function edit(Request $request, $local, Collection $collection) {

        return view('admin.collections.edit', compact('collection'));

    }

    public function store(StoreCollection $request) {

        $collection = new Collection($request->toArray());

        try {
            if ( ! $collection->save()) {
                return redirect('/tr/admin/collections/create')->with('error', Config::get('constants.messages.operation_error'))->withInput();
            };
        } catch (Exception $exception) {
            return redirect('/tr/admin/collections/create')->with('error', $exception->getMessage())->withInput();
        }

        if (isset($request->get('file')[0])) {
            $base64 = $request->get('file')[0];
            $extension = $this->getExtension($request->get('file')[0]);
            $path = "/collections/collection-" . uniqid() . $extension;
            file_put_contents(app_path() . '/../public' . $path, file_get_contents($base64));
            $collection->image = $path;
            $collection->save();
        }

        return redirect('/tr/admin/collections')->with('success', Config::get('constants.messages.operation_success'));

    }

    public function update(StoreCollection $request, $locale, Collection $collection) {

        if ($collection->update($request->toArray())) {

            if (isset($request->get('file')[0])) {
                $base64 = $request->get('file')[0];
                $extension = $this->getExtension($request->get('file')[0]);
                $path = "/collections/collection-" . uniqid() . $extension;
                file_put_contents(app_path() . '/../public' . $path, file_get_contents($base64));
                $collection->image = $path;
                $collection->save();
            } else {
                $collection->image = null;
                $collection->save();
            }

            return redirect('/tr/admin/collections')->with('success', Config::get('constants.messages.operation_success'));
        };

        return redirect('/tr/admin/collections')->with('success', Config::get('constants.messages.operation_error'));

    }

}
