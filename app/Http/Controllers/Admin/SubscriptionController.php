<?php

namespace App\Http\Controllers\Admin;

use App\Subscription;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {

        $this->middleware('auth');
    }

    public function index() {

        $subscriptions = Subscription::get();

        return view('admin.subscriptions.index', compact('subscriptions'));
    }

}
