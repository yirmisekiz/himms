<?php

namespace App\Http\Controllers;

use App\Age;
use App\Category;
use App\Color;
use App\Mail\ContactForm;
use App\News;
use App\Collection;
use App\Product;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PagesController extends Controller {

    /*
     * Cannonical url eklenmeli htmle
     */
    public function showCollection(Request $request, $local, $slug, $collectionId) {

        $collection = Collection::where('slug_tr', $slug)->whereNull('is_passive')->first();
        if (is_null($collection)) {
            $slugCollection = 'slug_en';
            $collection = Collection::where('slug_en', $slug)->whereNull('is_passive')->first();
        } else {
            $slugCollection = 'slug_tr';
        }

        if ( ! isset($collection)) {
            throw new NotFoundHttpException();
        }

        if (strval($collection->id) !== $collectionId) {
            throw new NotFoundHttpException();
        }

        $otherCollections = Collection::where('id', '!=', $collection->id)->get();
        if (is_null($collection)) {
            throw new NotFoundHttpException();
        }

        $colors = $collection->products->pluck('colors')->collapse()->pluck('id')->unique();
        $colors = Color::whereIn('id', $colors)->get();

        $availableAges = Age::whereIn('id', $collection->products->pluck('age_id')->unique())->get();

        $availableCategories = Category::whereIn('id', $collection->products->pluck('category_id')->unique())->get();

        $name = 'name_' . session('locale');

        return view('public.collection', compact('collection', 'name', 'colors', 'otherCollections', 'availableAges', 'slugCollection', 'availableCategories'));
    }

    public function showProduct(Request $request, $local, $collectionSlug, $collectionId, $productSlug, $productId) {

        $product = Product::where('slug_tr', $productSlug)->whereNull('is_passive')->where('id', $productId)->firstOrFail();
        if (strval($product->id) !== $productId) {
            throw new NotFoundHttpException();
        }

        $collection = Collection::where('slug_tr', $collectionSlug)->where('id', $collectionId)->first();
        if (is_null($collection)) {
            $slugCollection = 'slug_en';
            $collection = Collection::where('slug_en', $collectionSlug)->where('id', $collectionId)->first();
        } else {
            $slugCollection = 'slug_tr';
        }

        if (strval($collection->id) !== $collectionId) {
            throw new NotFoundHttpException();
        }

        $description = 'description_' . session('locale');
        $name = 'name_' . session('locale');

        $baseURL =  URL::to('');

        $otherProducts = $collection->products->where('id', '!=', $product->id);

        $pageTitle = $product->name;

        return view('public.product', compact('product', 'description', 'name', 'otherProducts', 'collection','slugCollection','baseURL','pageTitle'));
    }

    public function addSubscription(Request $request) {

        if ( ! Subscription::where('email', $request->get('email'))->exists()) {
            $subs = new Subscription();
            $subs->email = $request->get('email');
            $subs->save();
        };

        return redirect()->back();
    }

    public function sendEmail(Request $request) {

        Mail::to('info@himms.com.tr')->send(new ContactForm($request->all()));

        return redirect()->back();

    }

    public function showNews() {

        $news = News::get();
        $title = 'title_' . (session('locale'));
        $description = 'description_' . (session('locale'));

        return view('public.news', compact('news', 'title', 'description'));
    }

    public function showAbout() {

        return view('public.about');
    }

    public function showContactUs() {

        return view('public.contact_us');
    }

    public function showShops() {

        return view('public.shops');
    }

}
