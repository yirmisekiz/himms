<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getExtension($base64Data) {

        $data = explode(',', $base64Data);
        $ImageStream = base64_decode($data[1]);
        $contentType = finfo_buffer(finfo_open(), $ImageStream, FILEINFO_MIME_TYPE);

        $map = [
            'application/pdf' => '.pdf',
            'application/zip' => '.zip',
            'image/gif'       => '.gif',
            'image/jpeg'      => '.jpg',
            'image/png'       => '.png',
            'text/css'        => '.css',
            'text/html'       => '.html',
            'text/javascript' => '.js',
            'text/plain'      => '.txt',
            'text/xml'        => '.xml',
        ];
        if (isset($map[$contentType])) {
            return $map[$contentType];
        }

        // HACKISH CATCH ALL (WHICH IN MY CASE IS
        // PREFERRED OVER THROWING AN EXCEPTION)
        $pieces = explode('/', $contentType);

        return '.' . array_pop($pieces);
    }

}
