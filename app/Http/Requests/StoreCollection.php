<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCollection extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        return [
            'name_tr' => 'required|max:191'
        ];
    }

    public function messages() {

        return [
            'name_tr.required' => 'Türkçe koleksiyon adı giriniz.',
            'name_tr.max'      => 'Türkçe koleksiyon adı en fazla 191 karakter olmalıdır.',
        ];
    }

}
