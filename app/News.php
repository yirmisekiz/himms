<?php

namespace App;

use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model {

    use Notifiable;
    use SoftDeletes;

    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'title_tr',
            'title_ru',
            'title_sa',
            'title_en',
            'description_tr',
            'description_en',
            'description_ru',
            'description_sa',
            'image',
            'created_at',
            'updated_at',
            'deleted_at',
        ];

    protected $appends
        = [
            'created_at_formatted',
        ];

    public function getCreatedAtFormattedAttribute() {

        if (session('locale') === 'sa') {
            Date::setLocale('ar');
        } else {
            Date::setLocale(session('locale'));
        }

        return Date::createFromFormat('Y-m-d H:i', $this->created_at->format('Y-m-d H:i'))->format('j F Y');
    }

}
