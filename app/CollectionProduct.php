<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\Pivot;

class CollectionProduct extends Pivot {

    use SoftDeletes;

    protected $dates
        = [
            'created_at',
            'modified_at',
            'deleted_at'
        ];

    protected $fillable
        = [
            'collection_id',
            'product_id',
            'created_at',
            'modified_at',
            'deleted_at'
        ];

}
