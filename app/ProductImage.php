<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductImage extends Model {

    use SoftDeletes;

    protected $dates
        = [
            'created_at',
            'modified_at',
            'deleted_at'
        ];

    protected $fillable
        = [
            'path',
            'product_id',
            'created_at',
            'modified_at',
            'deleted_at'
        ];

}
