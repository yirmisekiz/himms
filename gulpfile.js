"use strict";

var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    maps = require('gulp-sourcemaps'),
     del = require('del'),
     autoprefixer = require('gulp-autoprefixer'),
     browserSync = require('browser-sync').create(),
     htmlreplace = require('gulp-html-replace'),
     cssmin = require('gulp-cssmin');

gulp.task("concatScripts", function() {
    return gulp.src([
        'public/js/vendor/jquery-3.3.1.min.js',
        'public/js/vendor/popper.min.js',
        'public/js/vendor/bootstrap.min.js'
        ])
    .pipe(maps.init())
    .pipe(concat('main.js'))
    .pipe(maps.write('./'))
    .pipe(gulp.dest('public/js'))
    .pipe(browserSync.stream());
});

gulp.task("minifyScripts", ["concatScripts"], function() {
  return gulp.src("public/js/main.js")
    .pipe(uglify())
    .pipe(rename('main.min.js'))
    .pipe(gulp.dest('docs/public/js'));
});

gulp.task('compileSass', function() {
  return gulp.src("public/css/main.scss")
      .pipe(maps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer())
      .pipe(maps.write('./'))
      .pipe(gulp.dest('public/css'))
      .pipe(browserSync.stream());
});

gulp.task("minifyCss", ["compileSass"], function() {
  return gulp.src("public/css/main.css")
    .pipe(cssmin())
    .pipe(rename('main.min.css'))
    .pipe(gulp.dest('docs/public/css'));
});

gulp.task('watchFiles', function() {
  gulp.watch('public/css/**/*.scss', ['compileSass']);
  gulp.watch('public/js/*.js', ['concatScripts']);
})

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('clean', function() {
  del(['docs', 'public/css/main.css*', 'public/js/main*.js*']);
});

gulp.task('renameSources', function() {
  return gulp.src('*.html')
    .pipe(htmlreplace({
        'js': 'public/js/main.min.js',
        'css': 'public/css/main.min.css'
    }))
    .pipe(gulp.dest('docs/'));
});

gulp.task("build", ['minifyScripts', 'minifyCss'], function() {
  return gulp.src(['*.html', '*.php','*.css','favicon.ico',
                   "public/img/**","public/css/theme.css","public/js/theme.js","public/fonts/**"], { base: './'})
            .pipe(gulp.dest('docs'));
});

gulp.task('serve', ['watchFiles'], function(){
  browserSync.init({
        server: "./"
    });

    gulp.watch("public/css/**/*.scss", ['watchFiles']);
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task("default", ["clean", 'build'], function() {
  gulp.start('renameSources');
});
