<?php

return [
    'messages' => [
        'operation_success' => 'Başarılı işlem',
        'operation_error'   => 'Hatalı işlem',
        'validation_failure'   => 'Lütfen bilgilerinizi kontrol edip, tekrar deneyiniz',
    ]
];
